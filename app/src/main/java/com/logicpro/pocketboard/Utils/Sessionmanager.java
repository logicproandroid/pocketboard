package com.logicpro.pocketboard.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class Sessionmanager {

    // Shared Preferences
    private SharedPreferences pref;

    // Editor for Shared preferences
    private SharedPreferences.Editor editor;

    // Context
    private Context mContext;
    // Shared pref mode
    private int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "Doctor8Pref";

    public static final String USER_LOGIN_ID = "userLoginId";
    public static final String USER_ID = "userId";
    public static final String USER_ROLE = "userRole";
    public static final String USER_NAME = "Mobile";
    public static final String REG_ID = "regId";
    public static final String ISLOGIN = "login";
    public  static  final  String DETAIL_STATUS="user_complete_information";
    public  static  final  String BRANCH_ID="branchId";
    public  static  final  String KEY_FCM_TOKEN="token";
    public  static  final  String  LATITUDE="latitude";
    public  static  final  String LONGITUDE="logitude";
    public static final String REMEMBER_PASSWORD="patientPassword";
    public static final String REMEMBER_ME = "remember_me";


    // public static final String KEY_FCM_TOKEN = "token";

    String Patient,Doctor;
    //Referrer ID
    public static final String REFERRER_ID = "referrer_id";
    /*___________________________________doctor_____________________________________________*/

    // Sharedpref file name
    //private static final String PREF_NAME = "Doctor8DocPref";

    public static final String DOC_LOGIN_ID = "docLoginId";
    public static final String DOC_ID = "docId";
    public static final String DOC_ROLE = "docRole";
    public static final String ISDOCTORLOGIN = "doclogin";
    public  static  final  String DOC_BRANCH_ID="doc_branchId";
    public static final String REMEMBER_DOC_PASSWORD="docPassword";
    public static final String REMEMBER_DOC_ME = "remember_me";

    public Sessionmanager(Context mcontext) {
        this.mContext = mcontext;
        pref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.commit();
        editor.apply();
    }

    public Sessionmanager() {

    }

    /**
     * Create session for patient
     */
    public void createUserLogin(String userId, String role,String regId,String Username) {
        //  editor.putString(USER_LOGIN_ID, loginId);
        editor.putString(USER_ID, userId);
        editor.putString(USER_ROLE, role);
        editor.putString(REG_ID, regId);
        editor.putString(USER_NAME, Username);
        //  editor.putString(DETAIL_STATUS, detailStatus);
        editor.putBoolean(ISLOGIN, true);
        editor.commit();
    }

    /**
     * Get user details
     */
    public HashMap getUserDetails() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        // hashMap.put(USER_LOGIN_ID, pref.getString(USER_LOGIN_ID, null));
        hashMap.put(USER_ID, pref.getString(USER_ID, null));
        hashMap.put(USER_ROLE, pref.getString(USER_ROLE, null));
        hashMap.put(USER_NAME, pref.getString(USER_NAME, null));
        hashMap.put(REG_ID, pref.getString(REG_ID, null));
        return hashMap;
    }
    /**
     * make Login
     */
   /* public void CheckLogin() {

        if (isLogin()) {
            if (pref.getString(USER_ROLE, null).equalsIgnoreCase("Patient")) {
                Intent i = new Intent(mContext, ActivityUserDashBoard.class);

                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Staring Login Activity
                mContext.startActivity(i);
            } else {
                Intent i = new Intent(mContext, ActivityPatientDocDashboard.class);

                // Closing all the Activities
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                // Add new Flag to start new Activity
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                // Staring Login Activity
                mContext.startActivity(i);
            }
        } else {
            // After logout redirect user to Login Activity
            Intent i = new Intent(mContext, ActivityPatientDocDashboard.class);

            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            mContext.startActivity(i);
        }

    }*/
    /**
     * make logout
     */
    public void CheckLogout() {
        editor.remove(USER_ID);
        editor.remove(REG_ID);
        editor.remove(USER_ROLE);
        editor.remove(USER_NAME);
        editor.remove(KEY_FCM_TOKEN);
        editor.remove(ISLOGIN);
        editor.commit();


        // After logout redirect user to Login Activity
        /*Intent i = new Intent(mContext, ActivityLogin.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        mContext.startActivity(i);*/
    }

    /**
     * method to check direct login
     */
    public boolean isPatientLogin() {
        return pref.getBoolean(ISLOGIN, false);
    }

    /**
     * Store Referrer Id
     */
    public void storeReferrerId(String referrerId) {
        editor.putString(REFERRER_ID, referrerId);
        editor.commit();
    }

    /**
     * To get referrer id while registering new user
     */
    public HashMap<String, String> getReferrerId() {
        HashMap<String, String> referrId = new HashMap<String, String>();
        referrId.put(REFERRER_ID, pref.getString(REFERRER_ID, null));
        return referrId;
    }


    public void saveLatLong(String latitude, String Longitude, String userId) {
        editor.putString(USER_ID, userId);
        editor.putString(LATITUDE, latitude);
        editor.putString(LONGITUDE, Longitude);
        editor.commit();
    }

    /**
     * Get user details
     */
    public HashMap getLatLong() {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        hashMap.put(USER_ID, pref.getString(USER_ID, null));
        hashMap.put(LATITUDE, pref.getString(LATITUDE, null));
        hashMap.put(LONGITUDE, pref.getString(LONGITUDE, null));
        return hashMap;
    }

    /**
     * Check remember me
     */
    public boolean isRememberMe() {
        return pref.getBoolean(REMEMBER_ME, false);
    }
    /**
     * Set remember me
     */
    public void setRememberMe(String usercode, String userPassword) {

        editor.putString(USER_LOGIN_ID, usercode);
        editor.putString(REMEMBER_PASSWORD, userPassword);
        editor.putBoolean(REMEMBER_ME, true);
        editor.commit();
    }
    /**
     * Get remember me
     */
    public HashMap<String, String> getRememberMe() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(USER_LOGIN_ID, pref.getString(USER_LOGIN_ID, null));
        user.put(REMEMBER_PASSWORD, pref.getString(REMEMBER_PASSWORD, null));
        return user;
    }

    /**
     * Clear remember me
     */
    public void clearRememberMe() {
        editor.remove(USER_LOGIN_ID);
        editor.remove(REMEMBER_PASSWORD);
        editor.putBoolean(REMEMBER_ME, false);
        editor.commit();
    }
    /*____________________common login check_________________________________________________________________________________*/

    public void checkLogin() {
        // Check login status
        if (this.isLoggedIn()) {

            Intent i = null;
            // user is logged in redirect him to Main Activity
            if (pref.getString(USER_ROLE, null).equalsIgnoreCase("user")) {
             //   i = new Intent(mContext, ActivityUserDashBoard.class);

            } else if (pref.getString(USER_ROLE, null).equalsIgnoreCase("doctor")) {
               // i = new Intent(mContext, ActivityDoctorDashboard.class);

            }

            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            mContext.startActivity(i);


        } else {
            // user is not logged in redirect him to Login Activity
           /* Intent i = new Intent(mContext, ActivityLogin.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            mContext.startActivity(i);*/
        }

    }

    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(ISLOGIN, false);
    }






    //this method will save the device token to shared preferences
    public boolean saveDeviceToken(String token) {
        editor.putString(KEY_FCM_TOKEN, token);
        editor.apply();
        return true;
    }



    /////

    //this method will fetch the device token from shared preferences
    public String getDeviceToken() {
        return pref.getString(KEY_FCM_TOKEN, null);
    }




    /*___________________doctor_____________________________________________________________________________*/
    /**
     * Create session for doctor
     */
    public void createDoctorSession(String docLoginId, String docId, String role,String branchId) {

        editor.putString(DOC_LOGIN_ID, docLoginId);
        editor.putString(DOC_ID, docId);
        editor.putString(DOC_ROLE, role);
        editor.putString(DOC_BRANCH_ID, branchId);
        editor.putBoolean(ISLOGIN, true);

        editor.commit();
    }
    /**
     * Get doctor details
     */
    public HashMap getDoctorDetails() {

        HashMap<String, String> hashMap = new HashMap<String, String>();

        hashMap.put(DOC_LOGIN_ID, pref.getString(DOC_LOGIN_ID, null));
        hashMap.put(DOC_ID, pref.getString(DOC_ID, null));
        hashMap.put(DOC_ROLE, pref.getString(DOC_ROLE, null));
        hashMap.put(DOC_BRANCH_ID, pref.getString(DOC_BRANCH_ID, null));


        return hashMap;
    }
    /**
     * make Login
     */



    /**
     * method to check direct login
     */
    public boolean isDocLogin() {
        return pref.getBoolean(ISDOCTORLOGIN, false);
    }

    /**
     * logout
     */
    public void CheckDoctorLogout() {
        editor.remove(DOC_LOGIN_ID);
        editor.remove(DOC_ID);
        editor.remove(DOC_ROLE);
        editor.remove(DOC_BRANCH_ID);
        editor.remove(ISLOGIN);
        editor.commit();


       /* // After logout redirect user to Login Activity
        Intent i = new Intent(mContext, ActivityLogin.class);

        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        mContext.startActivity(i);*/
    }

    /**
     * Check remember me
     */
    public boolean isDoctorRememberMe() {
        return pref.getBoolean(REMEMBER_DOC_ME, false);
    }
    /**
     * Set remember me
     */
    public void setDoctorRememberMe(String usercode, String userPassword) {

        editor.putString(DOC_LOGIN_ID, usercode);
        editor.putString(REMEMBER_DOC_PASSWORD, userPassword);
        editor.putBoolean(REMEMBER_DOC_ME, true);
        editor.commit();
    }
    /**
     * Get remember me
     */
    public HashMap<String, String> getDoctorRememberMe() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(DOC_LOGIN_ID, pref.getString(DOC_LOGIN_ID, null));
        user.put(REMEMBER_DOC_PASSWORD, pref.getString(REMEMBER_PASSWORD, null));
        return user;
    }

    /**
     * Clear remember me
     */
    public void clearDoctorRememberMe() {
        editor.remove(DOC_LOGIN_ID);
        editor.remove(REMEMBER_DOC_PASSWORD);
        editor.putBoolean(REMEMBER_DOC_ME, false);
        editor.commit();
    }
}
