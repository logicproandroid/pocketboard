package com.logicpro.pocketboard.Utils;

public class ConstantVariables {

    public static final int REQUEST_PERMISSION = 1001;
    public static final int PICK_CAMERA_IMAGE = 1002;
    public static final int PICK_GALLERY_IMAGE = 1003;

    /*Register & Login*/
    public static final int USER_REGISTER = 1;
    public static final int USER_LOGIN = 2;
    public static final int BOARD_LIST = 3;
    public static final int MUDIUM_LIST = 4;
    public  static  final  int CLASS_LIST=5;



}
