package com.logicpro.pocketboard.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.logicpro.pocketboard.R;


import org.jsoup.Jsoup;

public class CheckAppVersion {

    private Context mContext;

    public CheckAppVersion(Context mContext) {
        this.mContext = mContext;

        new GetLatestVersion().execute();
    }

    private class GetLatestVersion extends AsyncTask<String, String, String> {

        private boolean isAvailableInPlayStore;
        private String mStringCheckUpdate = "";
        private String currentVersion;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                currentVersion = pInfo.versionName;

                isAvailableInPlayStore = true;

                mStringCheckUpdate = Jsoup.connect("https://play.google.com/store/apps/details?id=" + mContext.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();

                return mStringCheckUpdate;
            } catch (Exception e) {
                e.printStackTrace();
                isAvailableInPlayStore = false;
                return mStringCheckUpdate;
            }
        }

        @Override
        protected void onPostExecute(String newVersion) {

            if (isAvailableInPlayStore) {
                Log.v("version", currentVersion);
                Log.v("version", newVersion);

                if (!currentVersion.equalsIgnoreCase(newVersion)) {
                    showUpdateDialog();
                }
            }
        }
    }

    private void showUpdateDialog() {
        // custom dialog
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.update_version_dialog);

        // set the custom dialog components - text, image and button
        TextView tvUpdateNow = dialog.findViewById(R.id.tvUpdateNow);
        // if button is clicked, close the custom dialog
        tvUpdateNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                mContext.startActivity(i);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    /*private void showUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mContext.getResources().getString(R.string.app_name))
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setMessage("New version is available.")
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String url = "https://play.google.com/store/apps/details?id=" + mContext.getPackageName();
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        mContext.startActivity(i);
                    }
                })
                .show();
    }*/
}
