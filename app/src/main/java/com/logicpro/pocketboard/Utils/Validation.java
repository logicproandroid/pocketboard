package com.logicpro.pocketboard.Utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.regex.Pattern;

public class Validation extends Activity {

	private static String USER_REGEX="^[A-Za-z_ ]{3,50}$";
	private static String USER_NAME="^[A-Za-z_ ]$";
	private static String APLPHABETSPACE="\"^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$\"";

	//private static String EMAIL_REGEX="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
	private static String EMAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	//private static String PHONE_REGEX = "\\d{5}\\d{7}";
	//private static String PASS_REGEX ="^.*(?=.{10})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$";
	private static String PASS_REGEX ="((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,10})";
	private static String PHONE_REGEX = "^[0-9]{10}$";
	static int i;
	static String text;
	// Error Messages
	private static final String REQUIRED_MSG = "Field Required";
	private static final String EMAIL_MSG = "INVALID EMAIL";
	private static final String PASS = "ATLEAST ONE DIGIT , ONE UPPER CASE & LOWER CASE CHARACTER ,ONE SPECIAL SYMBOL REQUIRED";
	private static final String PASS_MSG="INVALID PASSWORD";
	private static final String PHONE_MSG = "INVALID PHONE NO";
    Context c;
    // call this method when you need to check email validation
    public static boolean isEmailAddress(EditText editText, boolean required, int i) {
        return isValid(editText, EMAIL_REGEX, PASS, required,i);
    }
 
    // call this method when you need to check phone number validation
    public static boolean isPhoneNumber(EditText editText, boolean required, int i) {
        return isValid(editText, PASS_REGEX, PHONE_MSG, required,i);
    }

	public static boolean isPassword(EditText editText, boolean required, int i) {
		return isValid(editText, PHONE_REGEX, PHONE_MSG, required,i);
	}
 
    // return true if the input field is valid, based on the parameter passed
    public static boolean isValid(EditText editText, String regex, String errMsg, boolean required, int i) {
 
    	text = editText.getText().toString().trim();
       if(i==0)
       {
       
        // clearing the error, if it was previously set by some other values
        editText.setError(null);
 
        // text required and editText is blank, so return false
        if ( required && !hasText(editText) ) return false;
 
        // pattern doesn't match so returning false
         if (!Pattern.matches(regex, text)) {
            editText.setError(errMsg);
            return false;
        };
       }
       else 
       {
    	   
    	   if(text.length()==0)
           {
           	editText.setError(null);
           }
           else  if (!Pattern.matches(regex, text)) {
               editText.setError(errMsg);
               return false;
           };
       }
 
        return true;
    }



	public static boolean isValidName(EditText editText, boolean required)
	{
		String text = editText.getText().toString().trim();
		if(required)
		{


			if(text.length()==0)
			{
				editText.setError(REQUIRED_MSG);
				return false;
			}
			else if (text.length()<3) {
				editText.setError("Minimum 3 Characters Required");
				return false;
			}
			/*else  if(!Pattern.matches(APLPHABETSPACE,editText.getText().toString())) {
				editText.setError("ONLY CHARACTERS AND SPACE ALLOWED");
				return false;

			}*/
		}

		return true;
	}


	public static boolean isValidPhone(EditText editText, boolean required)
	{
		String text = editText.getText().toString().trim();
		if(required)
		{


			if(text.length()==0)
			{
				editText.setError("Mobile No. Required");
				return false;
			}
			else
			{
				if (!Pattern.matches(PHONE_REGEX, text)) {
					editText.setError(PHONE_MSG);
					return false;
				};
			}
		}
		else
		{
			if(text.length()!=0)
			{
				if (!Pattern.matches(PHONE_REGEX, text)) {
					editText.setError(PHONE_MSG);
					return false;
				};
			}
		}

		return true;
	}

	public static boolean isValidEmail(EditText editText, boolean required)
	{
		String text = editText.getText().toString().trim();
		if(required)
		{

			if(text.length()==0)
			{
				editText.setError("Email ID  Required");
				return false;
			}
			else
			{
				if (!Pattern.matches(EMAIL_REGEX, text)) {
					editText.setError(EMAIL_MSG);

					return false;
				};
			}
		}
		else
		{
			if((text.length()!=0) && (!Pattern.matches(EMAIL_REGEX, text)))
			{
				//if (!Pattern.matches(EMAIL_REGEX, text)) {
				editText.setError(EMAIL_MSG);

				return false;
				// };
			}
		}

		return true;
	}

	//Password
	public static boolean isValidPass(EditText editText, boolean required)
	{
		String text = editText.getText().toString().trim();
		if(required)
		{

			if(text.length()<4)
			{
				editText.setError("Minimum 4 Characters Required");

				return false;
			}
		}
		else
		{
			if(text.length()!=0 )
			{
				if (!Pattern.matches(PASS_REGEX, text)) {
					editText.setError(PASS);
					return false;
				};
			}
		}
		return true;
	}
	public static boolean isValidAmount(EditText editText, boolean required)
	{
		String text = editText.getText().toString().trim();
		if(required)
		{

			if(text.length()==0)
			{
				editText.setError(REQUIRED_MSG);

				return false;
			}
			else
			{
				if (text.equalsIgnoreCase("0")) {
					editText.setError("For benifit enter Amount More than 10");
					return false;
				};

			}
		}


		return true;
	}
	// check the input field has any text or not
    // return true if it contains text otherwise false
    public static boolean hasText(EditText editText) {
 
        String text = editText.getText().toString().trim();
        editText.setError(null);
        
        // length 0 means there is no text
        if (text.length() == 0) {
           editText.setError(REQUIRED_MSG);


            return false;
        }
    
        return true;
    }

    public static boolean hasSpinner(Spinner sp, Context c)
    {
    //	cd = new ConfirmationDialogs(c);
    	
    	//cd.messageToast(sp.getSelectedItem().toString());
    	if(sp.getSelectedItem().toString().contains("Select"))
    	{
    		Toast toast = Toast.makeText(c,"Invalid selection!", Toast.LENGTH_SHORT);
			TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
			v.setTextColor(Color.CYAN);
		//	v.setBackgroundColor(Color.WHITE);
			toast.show();
			return false;
    	}
		return true;
    }
    
    public static boolean toolTip(EditText editText, Context c, String msg) {
    	 
        String text = editText.getText().toString().trim();
        editText.setError(null);
        
        // length 0 means there is no text
        if (text.length() == 0) {
           //editText.setError(REQUIRED_MSG);
      
                editText.setError(msg, null);
                
            return false;
        }
    
        return true;
    }
    public void setListner(EditText et, final String msg, Context c)
    {
    et.setOnFocusChangeListener(new OnFocusChangeListener() {
		
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			// TODO Auto-generated method stub
		EditText et=(EditText)v;
		if(hasFocus)
		et.setError(msg,null);
			
		}
	});
	
    }
}
