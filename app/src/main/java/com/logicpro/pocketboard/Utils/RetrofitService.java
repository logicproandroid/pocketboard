package com.logicpro.pocketboard.Utils;

import android.content.Context;

import com.google.gson.JsonObject;
import com.logicpro.pocketboard.interfaces.IResult;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetrofitService {

    private IResult mResultCallback = null;
    private Context mContext;

    public RetrofitService(IResult resultCallback, Context context) {
        mResultCallback = resultCallback;
        mContext = context;
    }

    //Retrofit JsonObject
    public void retrofitData(final int requestId, Call<JsonObject> url) {
        try {

            //ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
            Call<JsonObject> call = url;

            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                    if (mResultCallback != null) {
                        mResultCallback.notifySuccess(requestId, response);
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    if (mResultCallback != null)
                        mResultCallback.notifyError(requestId, t);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}









