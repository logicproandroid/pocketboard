package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.model.BadgeForm;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RVBadgeAdapter extends RecyclerView.Adapter<RVBadgeAdapter.ItemViewHolder> {
    private  Context context;
    private  ArrayList<BadgeForm> badgeFormArrayLis;

    public RVBadgeAdapter(Context context, ArrayList<BadgeForm> badgeFormArrayList) {
        this.context=context;
        this.badgeFormArrayLis=badgeFormArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.badges_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvName.setText(badgeFormArrayLis.get(position).getName());
        if(position==0) {
             Picasso.with(context).load(R.drawable.trophy2).into(holder.imageView);
        }
        else if(position==1)
        {
            Picasso.with(context).load(R.drawable.trophy).into(holder.imageView);
        }else if(position==2)
        {
            Picasso.with(context).load(R.drawable.trophy1).into(holder.imageView);
        }



    }

    @Override
    public int getItemCount() {
        return badgeFormArrayLis.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private ImageView imageView;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_badge_name);
            imageView=itemView.findViewById(R.id.image_badge);
        }
    }
}
