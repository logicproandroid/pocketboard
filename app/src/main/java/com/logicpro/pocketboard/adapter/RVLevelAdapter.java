package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.ExamInformationActivity;
import com.logicpro.pocketboard.model.QuestionInfoModel;

import java.util.ArrayList;

public class RVLevelAdapter extends RecyclerView.Adapter<RVLevelAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<QuestionInfoModel> questionInfoModelArrayList;

    public RVLevelAdapter(Context mContext, ArrayList<QuestionInfoModel> questionInfoModelArrayList) {
        this.mContext = mContext;
        this.questionInfoModelArrayList = questionInfoModelArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if (position == 1) {
            holder.tvTestName.setText(questionInfoModelArrayList.get(position).getmQuestionName());
            holder.tvRank.setVisibility(View.VISIBLE);
            holder.tvMarks.setVisibility(View.VISIBLE);
            holder.tvMarks.setTextColor(mContext.getResources().getColor(R.color.colorAccent));


            holder.tvTimeUp.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.GONE);

        } else if (position == 0) {
            holder.tvTestName.setText(questionInfoModelArrayList.get(position).getmQuestionName());
            holder.tvRank.setVisibility(View.GONE);
            holder.tvMarks.setVisibility(View.VISIBLE);
            holder.tvTimeUp.setVisibility(View.GONE);
            holder.tvMarks.setText("100M | 15 Min | 30 Question ");
            holder.tvMarks.setTextColor(mContext.getResources().getColor(R.color.purple_700));
            holder.imageView.setVisibility(View.VISIBLE);
           // holder.cardView.setCardBackgroundColor(mContext.getResources().getColor(R.color.cardbg));


            holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, ExamInformationActivity.class);
                    mContext.startActivity(intent);
                }
            });


        } else if (position == 2) {
            holder.tvTestName.setText(questionInfoModelArrayList.get(position).getmQuestionName());
            holder.tvRank.setVisibility(View.VISIBLE);
            holder.tvRank.setText("Time Up");
            holder.tvRank.setTextColor(mContext.getResources().getColor(R.color.colorRed));

            holder.tvMarks.setVisibility(View.VISIBLE);
            holder.tvMarks.setText("Marks Obitained : 0");
            holder.tvTimeUp.setVisibility(View.GONE);
            holder.imageView.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return questionInfoModelArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTestName, tvMarks, tvTimeUp, tvRank;
        private ImageView imageView;
        private RelativeLayout relativeLayout;
        private CardView cardView;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTestName = itemView.findViewById(R.id.tv_test_name);
            tvMarks = itemView.findViewById(R.id.tv_marks);

            tvTimeUp = itemView.findViewById(R.id.tv_time_up);

            tvRank = itemView.findViewById(R.id.tv_rank);
            imageView = itemView.findViewById(R.id.img);
            relativeLayout = itemView.findViewById(R.id.relative_layout);
            cardView=itemView.findViewById(R.id.cardview);

        }
    }
}
