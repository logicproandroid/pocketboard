package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.TeacherUploadYoutubelink;
import com.logicpro.pocketboard.model.SubYouTubeForm;
import com.logicpro.pocketboard.model.TeacherYoutubeLinkForm;

import java.util.ArrayList;

public class RVYoutubeUploadAdapter  extends RecyclerView.Adapter<RVYoutubeUploadAdapter.ItemViewHolder>{
    private Context mContext;
    private  ArrayList<TeacherYoutubeLinkForm> subYouTubeFormArrayList;

    public RVYoutubeUploadAdapter(Context mContext, ArrayList<TeacherYoutubeLinkForm> subYouTubeArrayList) {
        this.mContext=mContext;
        this.subYouTubeFormArrayList=subYouTubeArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.youtube_upload_link_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvSubName.setText(subYouTubeFormArrayList.get(position).getTopic());
        holder.tvBoard.setText(subYouTubeFormArrayList.get(position).getBoard()+ " "+"("+subYouTubeFormArrayList.get(position).getMedium()+")");
        holder.tvLink.setText(subYouTubeFormArrayList.get(position).getLink());
        holder.tvDate.setText(subYouTubeFormArrayList.get(position).getUpdateDate());
        holder.tvTime.setText(subYouTubeFormArrayList.get(position).getTime());
    }

    @Override
    public int getItemCount() {
        return subYouTubeFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubName,tvBoard,tvLink,tvDate,tvTime;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubName=itemView.findViewById(R.id.sub_name);
            tvBoard=itemView.findViewById(R.id.tv_board);
          tvLink=itemView.findViewById(R.id.tv_link_info);
            tvDate=itemView.findViewById(R.id.tv_date);
            tvTime=itemView.findViewById(R.id.tv_time);
        }
    }
}
