package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.TeacherTimeTableActivity;
import com.logicpro.pocketboard.model.TeacherTimeTable;

import java.util.ArrayList;

public class RvTimeTableAdapter extends RecyclerView.Adapter<RvTimeTableAdapter.ItemViewHolder>{
    private ArrayList<TeacherTimeTable>  teacherTimeTableArrayList;
    private Context mContext;

    public RvTimeTableAdapter(Context mContext, ArrayList<TeacherTimeTable> teacherTimeTableArrayList) {
        this.mContext=mContext;
        this.teacherTimeTableArrayList=teacherTimeTableArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_timetable_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        if(position==0) {
            holder.time.setText(teacherTimeTableArrayList.get(position).getTime());
            holder.subName.setText(teacherTimeTableArrayList.get(position).getSubjectName());
            holder.board.setText(teacherTimeTableArrayList.get(position).getBorad()+ " "+"("+teacherTimeTableArrayList.get(position).getMedium()+")");

            holder.calltype.setText(teacherTimeTableArrayList.get(position).getCallType());
            holder.cardView.setCardBackgroundColor(R.drawable.blue);
        }else
        {
            holder.time.setText(teacherTimeTableArrayList.get(position).getTime());
            holder.subName.setText(teacherTimeTableArrayList.get(position).getSubjectName());
            holder.board.setText(teacherTimeTableArrayList.get(position).getBorad()+ " "+"("+teacherTimeTableArrayList.get(position).getMedium()+")");

            holder.calltype.setText(teacherTimeTableArrayList.get(position).getCallType());
            //  holder.cardView.setBackgroundResource(R.color.pink);
            holder.cardView.setCardBackgroundColor(R.drawable.rounded_corner_pink);
        }

    }

    @Override
    public int getItemCount() {
        return teacherTimeTableArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView subName,board,time,calltype;
        private CardView cardView;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            subName=itemView.findViewById(R.id.tv_sub);
            board=itemView.findViewById(R.id.tv_techer);
            time=itemView.findViewById(R.id.tv_time);
            calltype=itemView.findViewById(R.id.tv_app);
            cardView=itemView.findViewById(R.id.cardview);


        }
    }
}
