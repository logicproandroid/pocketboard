package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.model.StatisticsModel;

import java.util.ArrayList;

public class RVStatisticsAdapter extends RecyclerView.Adapter<RVStatisticsAdapter.ItemViewHolder> {
    private Context mContext;
private ArrayList<StatisticsModel> statisticsModelArrayList;

    public RVStatisticsAdapter(Context context, ArrayList<StatisticsModel> statisticsArrayList) {
        this.mContext=context;
        this.statisticsModelArrayList=statisticsArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.statistics_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvName.setText(statisticsModelArrayList.get(position).getTestName());
        holder.tvScore.setText(statisticsModelArrayList.get(position).getScore());

    }

    @Override
    public int getItemCount() {
        return statisticsModelArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvScore;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_score);
            tvScore=itemView.findViewById(R.id.tv_test_name);
        }
    }
}
