package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.OnlineClassSubActivity;
import com.logicpro.pocketboard.model.SubYouTubeForm;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RVSubjectYoutubeAdapter extends RecyclerView.Adapter<RVSubjectYoutubeAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<SubYouTubeForm> subYouTubeFormArrayList;

    public RVSubjectYoutubeAdapter(Context context, ArrayList<SubYouTubeForm> subYouTubeArrayList) {
        this.mContext=context;
        this.subYouTubeFormArrayList=subYouTubeArrayList;
    }


    @NonNull
    @Override
    public RVSubjectYoutubeAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_youtube_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RVSubjectYoutubeAdapter.ItemViewHolder holder, int position) {
        holder.tvTopicName.setText(subYouTubeFormArrayList.get(position).getTeacherName());
        holder.tvDate.setText(subYouTubeFormArrayList.get(position).getUpdateDate());
        holder.tvTopicName.setText(subYouTubeFormArrayList.get(position).getTopic());
        Picasso.with(mContext).load(subYouTubeFormArrayList.get(position).getImage()).into(holder.imageView);


    }

    @Override
    public int getItemCount() {
        return subYouTubeFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView tvTeacherName,tvTopicName,tvDate;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTeacherName=itemView.findViewById(R.id.tv_teacher_name);
            imageView=itemView.findViewById(R.id.image);
            tvDate=itemView.findViewById(R.id.tv_date);
            tvTopicName=itemView.findViewById(R.id.tv_topic);
        }
    }
}
