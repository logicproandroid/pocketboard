package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.model.LeaderBoard;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RVLeaderBoardAdapter  extends RecyclerView.Adapter<RVLeaderBoardAdapter.ItemViewHolder> {
    private  Context mContext;
    private  ArrayList<LeaderBoard> leaderBoardArrayList;

    public RVLeaderBoardAdapter(Context conext, ArrayList<LeaderBoard> leaderBoardArrayList) {
        this.mContext=conext;
        this.leaderBoardArrayList=leaderBoardArrayList;
    }

    @NonNull
    @Override
    public RVLeaderBoardAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.leaderboard_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RVLeaderBoardAdapter.ItemViewHolder holder, int position) {
        holder.tvName.setText(leaderBoardArrayList.get(position).getName());
        holder.tvScore.setText(leaderBoardArrayList.get(position).getScore());
        // Picasso.with(context).load(badgeFormArrayLis.get(position).getImage()).into(holder.imageView);
        if(position==0) {
holder.relativeLayout1.setVisibility(View.VISIBLE);
holder.relativeLayout2.setVisibility(View.GONE);
            Picasso.with(mContext).load(R.drawable.gold_medal).into(holder.imageView);
            holder.tvMedal.setVisibility(View.GONE);
        }else if(position==1)
        {
            holder.relativeLayout1.setVisibility(View.VISIBLE);
            holder.relativeLayout2.setVisibility(View.GONE);
            Picasso.with(mContext).load(R.drawable.silver_medal).into(holder.imageView);
            holder.tvMedal.setVisibility(View.GONE);
        }
        else  if(position==2)
        {
            holder.relativeLayout1.setVisibility(View.VISIBLE);
            holder.relativeLayout2.setVisibility(View.GONE);
            Picasso.with(mContext).load(R.drawable.bronze_medal).into(holder.imageView);
            holder.tvMedal.setVisibility(View.GONE);
        }
        else if(position==3)
        {
            holder.relativeLayout1.setVisibility(View.GONE);
            holder.relativeLayout2.setVisibility(View.VISIBLE);
            holder.tvMedal.setVisibility(View.VISIBLE);
           // holder.imageView.setVisibility(View.GONE);
            holder.tvMedal.setText("6");
            holder.tvName1.setText(leaderBoardArrayList.get(position).getName());
            holder.tvScore1.setText(leaderBoardArrayList.get(position).getScore());
        }


    }

    @Override
    public int getItemCount() {
        return leaderBoardArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName,tvScore,tvMedal,tvName1,tvScore1;
        private ImageView imageView,imageView1;
        private CircleImageView circleImageView,circleImageView1;
        private RelativeLayout relativeLayout1,relativeLayout2;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName=itemView.findViewById(R.id.tv_name);
            tvScore=itemView.findViewById(R.id.tv_score);
            tvName1=itemView.findViewById(R.id.tv_name1);
            tvScore1=itemView.findViewById(R.id.tv_score1);
            imageView=itemView.findViewById(R.id.image_medal);
            circleImageView=itemView.findViewById(R.id.img_profile);
            circleImageView1=itemView.findViewById(R.id.img_profile1);
            tvMedal=itemView.findViewById(R.id.tv_medal);
            relativeLayout1=itemView.findViewById(R.id.relative_layout1);
            relativeLayout2=itemView.findViewById(R.id.relative_layout2);
        }
    }
}
