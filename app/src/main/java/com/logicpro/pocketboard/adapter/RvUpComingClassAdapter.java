package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.OnlineActivity4;
import com.logicpro.pocketboard.model.UpcomingClassForm;

import java.util.ArrayList;

public class RvUpComingClassAdapter  extends RecyclerView.Adapter<RvUpComingClassAdapter.ItemViewHolder> {
    private  Context mContext;
    private  ArrayList<UpcomingClassForm> upcomingClassFormArrayList;

    public RvUpComingClassAdapter(Context mContext, ArrayList<UpcomingClassForm> upcomingClassFormArrayList) {
   this.mContext=mContext;
   this.upcomingClassFormArrayList=upcomingClassFormArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_class_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        if(position==0) {
            holder.tvTime.setText(upcomingClassFormArrayList.get(position).getTime());
            holder.tvSub.setText(upcomingClassFormArrayList.get(position).getSubject());
            holder.tvTeacherName.setText(upcomingClassFormArrayList.get(position).getTeacherName());
            holder.tvApp.setText(upcomingClassFormArrayList.get(position).getAppName());
            holder.cardView.setCardBackgroundColor(R.drawable.blue);
        }else
        {
            holder.tvTime.setText(upcomingClassFormArrayList.get(position).getTime());
            holder.tvSub.setText(upcomingClassFormArrayList.get(position).getSubject());
            holder.tvTeacherName.setText(upcomingClassFormArrayList.get(position).getTeacherName());
            holder.tvApp.setText(upcomingClassFormArrayList.get(position).getAppName());
          //  holder.cardView.setBackgroundResource(R.color.pink);
            holder.cardView.setCardBackgroundColor(R.drawable.rounded_corner_pink);
        }

    }

    @Override
    public int getItemCount() {
        return upcomingClassFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTime,tvSub,tvTeacherName,tvApp,tvDate;
        private CardView cardView;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTime=itemView.findViewById(R.id.tv_time);
            tvSub=itemView.findViewById(R.id.tv_sub);
            tvTeacherName=itemView.findViewById(R.id.tv_techer);
            tvApp=itemView.findViewById(R.id.tv_app);
            cardView=itemView.findViewById(R.id.cardview);
            //tvDate=itemView.findViewById(R.id.tv_date);
        }
    }
}

