package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.OnlineClassSubActivity;
import com.logicpro.pocketboard.activity.TeacherMockTestActivity;
import com.logicpro.pocketboard.model.SubjectForm;

import java.util.ArrayList;
import java.util.Random;

public class RVSubjectUploardAdapter  extends RecyclerView.Adapter<RVSubjectUploardAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<SubjectForm> subjectFormArrayList;

    public RVSubjectUploardAdapter(Context mContext, ArrayList<SubjectForm> subjectFormArrayList) {
        this.mContext=mContext;
        this.subjectFormArrayList=subjectFormArrayList;
    }


    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subject_list, parent, false);
        ItemViewHolder vh =new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        holder.tvSubName.setText(subjectFormArrayList.get(position).getSubname());
        if (position == 0) {
            holder.imageView.setBackgroundResource(R.drawable.ic_english);
        } else if (position == 1) {
            holder.imageView.setBackgroundResource(R.drawable.ic_geometry);
            // Picasso.with(mContext).load(R.drawable.ic_evs).into(holder.imageView);
        } else if (position == 2) {
            holder.imageView.setBackgroundResource(R.drawable.ic_hindi);
            // Picasso.with(mContext).load(R.drawable.ic_hindi).into(holder.imageView);
        }

        holder.cardView.setCardBackgroundColor(getRandomColorCode(mContext));
    }

    private int getRandomColorCode(Context context) {
       /* int[] colors;
        if (Math.random() >= 0.6) {
            colors = context.getResources().getIntArray(R.array.note_accent_colors);
        } else {
            colors = context.getResources().getIntArray(R.array.note_neutral_colors);
        }
        return colors[((int) (Math.random() * colors.length))];

*/

        Random random = new Random();

        return Color.argb(255, random.nextInt(256), random.nextInt(256),     random.nextInt(256));
    }


    @Override
    public int getItemCount() {
        return subjectFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubName;
        private ImageView imageView;

        private CardView cardView;


        ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubName=itemView.findViewById(R.id.tv_sub_name);
            imageView=itemView.findViewById(R.id.image_sub);
            cardView=itemView.findViewById(R.id.cardview);

            /*itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, OnlineClassSubActivity.class);
                    mContext.startActivity(intent);
                }
            });*/


        }
    }
}
