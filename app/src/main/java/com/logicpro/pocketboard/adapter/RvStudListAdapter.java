package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.model.StudentForm;

import java.util.ArrayList;

public class RvStudListAdapter extends RecyclerView.Adapter<RvStudListAdapter.ItemViewHolder> {

    private Context mContext;
    private ArrayList<StudentForm> studentFormArrayList;

    public RvStudListAdapter(Context mContext, ArrayList<StudentForm> studentFormArrayList) {
        this.mContext = mContext;
        this.studentFormArrayList = studentFormArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.student_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvName.setText(studentFormArrayList.get(position).getName());
      //  holder.tvMedium.setText(studentFormArrayList.get(position).getMedium());
        holder.tvClass.setText(studentFormArrayList.get(position).getClassInfo());
      //  holder.tvBoard.setText(studentFormArrayList.get(position).getBorad());
        holder.tvSubscriptionDate.setText(studentFormArrayList.get(position).getSubscriptionDate());


    }

    @Override
    public int getItemCount() {
        return studentFormArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName, tvClass, tvBoard, tvMedium, tvSubscriptionDate;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_stud_name);
            tvClass = itemView.findViewById(R.id.tv_class);
            //tvBoard = itemView.findViewById(R.id.);
           // tvMedium = itemView.findViewById(R.id.);
            tvSubscriptionDate = itemView.findViewById(R.id.tv_date);
        }
    }
}