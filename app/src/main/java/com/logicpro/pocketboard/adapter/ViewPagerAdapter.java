package com.logicpro.pocketboard.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.logicpro.pocketboard.fragments.FragementBadges;
import com.logicpro.pocketboard.fragments.FragmentLeaderBoard;
import com.logicpro.pocketboard.fragments.FragmentStatisticsInfo;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentStatisticsInfo();
        }
        else if (position == 1)
        {
            fragment = new FragementBadges();
        }
        else if (position == 2)
        {
            fragment = new FragmentLeaderBoard();
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "Statistics";
        }
        else if (position == 1)
        {
            title = "Badges";
        }
        else if (position == 2)
        {
            title = "LeaderBoard";
        }
        return title;
    }
}
