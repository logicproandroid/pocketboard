package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.TeacherUploadSubjectActivity;
import com.logicpro.pocketboard.model.TeacherUploadSubDisplay;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Random;

public class RvTeacherSUbDisplayAdapter  extends RecyclerView.Adapter<RvTeacherSUbDisplayAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<TeacherUploadSubDisplay> teacherUploadSubDisplayArrayList;

    public RvTeacherSUbDisplayAdapter(Context mContext, ArrayList<TeacherUploadSubDisplay> teacherUploadSubDisplayArrayList) {
   this.mContext=mContext;
   this.teacherUploadSubDisplayArrayList=teacherUploadSubDisplayArrayList;

    }

    @NonNull
    @Override
    public RvTeacherSUbDisplayAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_sub_uploaded_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RvTeacherSUbDisplayAdapter.ItemViewHolder holder, int position) {
        holder.tvSubjectName.setText(teacherUploadSubDisplayArrayList.get(position).getmSubjectName());
        holder.tvBoard.setText(teacherUploadSubDisplayArrayList.get(position).getmBoradName()+ " "+"("+teacherUploadSubDisplayArrayList.get(position).getmMudiumName()+", "+teacherUploadSubDisplayArrayList.get(position).getmClassName()+")");
        holder.tvTotChapter.setText(teacherUploadSubDisplayArrayList.get(position).getTotalChater());

        //Picasso.with(mContext).load(teacherUploadSubDisplayArrayList.get(position).getIcon()).into(holder.image);

        if(position==0) {
            holder.image.setBackgroundResource(R.drawable.ic_english);
        }else if(position==1)
        {
            holder.image.setBackgroundResource(R.drawable.ic_geometry);
            // Picasso.with(mContext).load(R.drawable.ic_evs).into(holder.imageView);
        }else if(position==2)
        {
            holder.image.setBackgroundResource(R.drawable.ic_hindi);
            // Picasso.with(mContext).load(R.drawable.ic_hindi).into(holder.imageView);
        }

        holder.linearLayout.setBackgroundColor(getRandomColorCode(mContext));

    }

    private int getRandomColorCode(Context context) {
       /* int[] colors;
        if (Math.random() >= 0.6) {
            colors = context.getResources().getIntArray(R.array.note_accent_colors);
        } else {
            colors = context.getResources().getIntArray(R.array.note_neutral_colors);
        }
        return colors[((int) (Math.random() * colors.length))];

*/

        Random random = new Random();

        return Color.argb(255, random.nextInt(256), random.nextInt(256),     random.nextInt(256));
    }

    @Override
    public int getItemCount() {
        return teacherUploadSubDisplayArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubjectName,tvBoard,tvClass,tvMedium,tvTotChapter;
        private ImageView image;
        private LinearLayout linearLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubjectName=itemView.findViewById(R.id.tv_sub);
            tvBoard=itemView.findViewById(R.id.tv_borad);
           // tvClass=itemView.findViewById(R.id.);
            linearLayout=itemView.findViewById(R.id.linear);
            tvTotChapter=itemView.findViewById(R.id.tv_chapter);
            image=itemView.findViewById(R.id.image);
        }
    }
}
