package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.ActivityUploadVideo1;
import com.logicpro.pocketboard.activity.DashBoardTeacherActivity;
import com.logicpro.pocketboard.activity.TeacherUploadYoutubelink;
import com.logicpro.pocketboard.model.SubjectClassModel;

import java.util.ArrayList;
import java.util.Random;

public class RVSubjectClassAdapter extends RecyclerView.Adapter<RVSubjectClassAdapter.ItemViewHolder> {
    private  ArrayList<SubjectClassModel>  subjectClassModelArrayList;
    private  Context mContext;
    public RVSubjectClassAdapter(Context context, ArrayList<SubjectClassModel> subjectFormArrayList) {
        this.mContext=context;
        this.subjectClassModelArrayList=subjectFormArrayList;
    }

    @NonNull
    @Override
    public RVSubjectClassAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.teacher_sub_clas_list, parent, false);
       ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RVSubjectClassAdapter.ItemViewHolder holder, int position) {
        holder.tvSubjectName.setText(subjectClassModelArrayList.get(position).getSubname());
        holder.tvBoard.setText(subjectClassModelArrayList.get(position).getMclass());
     //   holder.tvTotChapter.setText(teacherUploadSubDisplayArrayList.get(position).getTotalChater());

        //Picasso.with(mContext).load(teacherUploadSubDisplayArrayList.get(position).getIcon()).into(holder.image);

        if(position==0) {
            holder.image.setBackgroundResource(R.drawable.ic_english);
        }else if(position==1)
        {
            holder.image.setBackgroundResource(R.drawable.ic_geometry);
            // Picasso.with(mContext).load(R.drawable.ic_evs).into(holder.imageView);
        }else if(position==2)
        {
            holder.image.setBackgroundResource(R.drawable.ic_hindi);
            // Picasso.with(mContext).load(R.drawable.ic_hindi).into(holder.imageView);
        }

        holder.linearLayout.setBackgroundColor(getRandomColorCode(mContext));

    }

    private int getRandomColorCode(Context context) {
       /* int[] colors;
        if (Math.random() >= 0.6) {
            colors = context.getResources().getIntArray(R.array.note_accent_colors);
        } else {
            colors = context.getResources().getIntArray(R.array.note_neutral_colors);
        }
        return colors[((int) (Math.random() * colors.length))];

*/

        Random random = new Random();

        return Color.argb(255, random.nextInt(256), random.nextInt(256),     random.nextInt(256));
    }

    @Override
    public int getItemCount() {
        return subjectClassModelArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSubjectName,tvBoard,tvClass,tvMedium,tvTotChapter;
        private ImageView image;
        private LinearLayout linearLayout;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSubjectName=itemView.findViewById(R.id.tv_sub);
            tvBoard=itemView.findViewById(R.id.tv_class);
            // tvClass=itemView.findViewById(R.id.);
            linearLayout=itemView.findViewById(R.id.linear);
          //  tvTotChapter=itemView.findViewById(R.id.tv_chapter);
            image=itemView.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent =new Intent(mContext, TeacherUploadYoutubelink.class);
                    mContext.startActivity(intent);
                }
            });
        }
    }
}
