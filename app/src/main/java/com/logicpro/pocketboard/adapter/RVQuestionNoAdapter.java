package com.logicpro.pocketboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.QuestionActivity;
import com.logicpro.pocketboard.model.QuestionNo;

import java.util.ArrayList;

public class RVQuestionNoAdapter  extends RecyclerView.Adapter<RVQuestionNoAdapter.ItemViewHolder> {
    private Context mContext;
    private ArrayList<QuestionNo> questionNoArrayList;

    public RVQuestionNoAdapter(Context mContext, ArrayList<QuestionNo> questionNoArrayList) {
        this.mContext=mContext;
        this.questionNoArrayList=questionNoArrayList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_no_list, parent, false);
        ItemViewHolder vh = new ItemViewHolder(view); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.tvQuestionNo.setText(questionNoArrayList.get(position).getQuestionNo());
        holder.tvQuestionNo.setBackgroundResource(R.drawable.circle_stroke);


    }

    @Override
    public int getItemCount() {
        return questionNoArrayList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private TextView tvQuestionNo;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvQuestionNo=itemView.findViewById(R.id.tv_question_no);
        }
    }
}
