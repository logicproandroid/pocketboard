package com.logicpro.pocketboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVStatisticsAdapter;
import com.logicpro.pocketboard.model.LeaderBoard;
import com.logicpro.pocketboard.model.StatisticsModel;

import java.util.ArrayList;

public  class FragmentStatisticsInfo extends Fragment {
    private View view;
    private RecyclerView rvStatisticsInfo;
    private ArrayList<StatisticsModel> statisticsArrayList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_statistics_info, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        statisticsArrayList.add(new StatisticsModel("0", "Maths","75%"));
        statisticsArrayList.add(new StatisticsModel("1", "Maths 1", "80%"));
        statisticsArrayList.add(new StatisticsModel("2", "Maths 2", "90%"));
        statisticsArrayList.add(new StatisticsModel("3", "Maths 3", "60%"));
        statisticsArrayList.add(new StatisticsModel("4", "Maths 4", "80%"));
        statisticsArrayList.add(new StatisticsModel("5", "Maths 5", "75%"));

        if (statisticsArrayList != null && statisticsArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),3);
            rvStatisticsInfo.setLayoutManager(gridLayoutManager);
            RVStatisticsAdapter rvStatisticsAdapter = new RVStatisticsAdapter(getActivity(), statisticsArrayList);
            rvStatisticsInfo.setHasFixedSize(true);
            rvStatisticsInfo.setNestedScrollingEnabled(false);
            rvStatisticsInfo.setAdapter(rvStatisticsAdapter);
        }
    }

    private void init() {
        rvStatisticsInfo=view.findViewById(R.id.rv_progress);
        statisticsArrayList=new ArrayList<>();
    }
}
