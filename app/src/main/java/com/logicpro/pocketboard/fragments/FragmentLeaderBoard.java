package com.logicpro.pocketboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVLeaderBoardAdapter;
import com.logicpro.pocketboard.model.LeaderBoard;

import java.util.ArrayList;

public class FragmentLeaderBoard extends Fragment {

    private View view;
    private RecyclerView rvLeadeBoard;
    private ArrayList<LeaderBoard> leaderBoardArrayList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_leaderboard, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        leaderBoardArrayList.add(new LeaderBoard("0", "Ram Patil","R.drawable.indiandoctor","","500"));
        leaderBoardArrayList.add(new LeaderBoard("1", "Jeevika Shinde", "R.drawable.indiandoctor"," ","300"));
        leaderBoardArrayList.add(new LeaderBoard("2", "Ragini kinekar", "R.drawable.indiandoctor","","200"));
        leaderBoardArrayList.add(new LeaderBoard("3", "kaveri kate", "R.drawable.indiandoctor","","100"));


        if (leaderBoardArrayList != null && leaderBoardArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(),RecyclerView.VERTICAL ,false);
            rvLeadeBoard.setLayoutManager(linearLayoutManager);
            RVLeaderBoardAdapter rvLeaderBoardAdapter = new RVLeaderBoardAdapter(getActivity(), leaderBoardArrayList);
            rvLeadeBoard.setHasFixedSize(true);
            rvLeadeBoard.setNestedScrollingEnabled(false);
            rvLeadeBoard.setAdapter(rvLeaderBoardAdapter);
        }
    }

    private void init() {
        leaderBoardArrayList=new ArrayList<>();
        rvLeadeBoard=view.findViewById(R.id.rv_leaderboard);
    }


}


