package com.logicpro.pocketboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVBadgeAdapter;
import com.logicpro.pocketboard.model.BadgeForm;

import java.util.ArrayList;

public class FragementBadges extends Fragment {
    private  View view;
    private RecyclerView rvBadges;
    private ArrayList<BadgeForm> badgeFormArrayList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_badges, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        badgeFormArrayList.add(new BadgeForm("0", "English","R.drawable.indiandoctor"));
        badgeFormArrayList.add(new BadgeForm("1", "Maths", "R.drawable.indiandoctor"));
        badgeFormArrayList.add(new BadgeForm("2", "History", "R.drawable.indiandoctor"));

        if (badgeFormArrayList != null && badgeFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            GridLayoutManager linearLayoutManager = new GridLayoutManager(getActivity(), 3);
            rvBadges.setLayoutManager(linearLayoutManager);
            RVBadgeAdapter rvBadgeAdapter = new RVBadgeAdapter(getActivity(), badgeFormArrayList);
            rvBadges.setHasFixedSize(true);
            rvBadges.setNestedScrollingEnabled(false);
            rvBadges.setAdapter(rvBadgeAdapter);
        }
    }

    private void init() {
        badgeFormArrayList=new ArrayList<>();
        rvBadges=view.findViewById(R.id.rv_badge);
    }


}
