package com.logicpro.pocketboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.ActivityChangePassword;
import com.logicpro.pocketboard.activity.ActivitySubscriprion;
import com.logicpro.pocketboard.activity.ProfileActivity;
import com.logicpro.pocketboard.activity.SignUpActivity;
import com.logicpro.pocketboard.model.LeaderBoard;

import java.util.ArrayList;

public class SettingFragment extends Fragment {

    private View view;
    private Toolbar toolbar;
    private RelativeLayout relativeLayout,relativeLayoutSub,relativeLayoutSecurity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_setting, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        setUpoolBar();
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("flag","flag");
                startActivity(intent);
            }
        });

        relativeLayoutSub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ActivitySubscriprion.class);
                startActivity(intent);
            }
        });


        relativeLayoutSecurity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), ActivityChangePassword.class);
                startActivity(intent);
            }
        });
    }

    private void setUpoolBar() {
        toolbar.setTitle("Settings");
      //  getActivity().setSupportActionBar(toolbar);
       // getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getActivity().getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

    }

    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        relativeLayout=view.findViewById(R.id.relative_layout_profile);
        relativeLayoutSub=view.findViewById(R.id.relative_layout_subscription);
        relativeLayoutSecurity=view.findViewById(R.id.relative_layout_security);
    }
}
