package com.logicpro.pocketboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.OnlineClassActivity2;
import com.logicpro.pocketboard.adapter.SliderAdapter;
import com.logicpro.pocketboard.model.SliderData;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class ExtraTestFragment extends Fragment {
    private  View view;
    private ImageView buttonBook;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.activity_online_class1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
       init();
       buttonBook.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent=new Intent(getActivity(), OnlineClassActivity2.class);
               startActivity(intent);

           }
       });



                // we are creating array list for storing our image urls.
                ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();

                // initializing the slider view.
                SliderView sliderView = view.findViewById(R.id.slider);

                // adding the urls inside array list
                sliderDataArrayList.add(new SliderData(R.drawable.banner_11));
                sliderDataArrayList.add(new SliderData(R.drawable.banner_12));
                sliderDataArrayList.add(new SliderData(R.drawable.banner13));

                // passing this array list inside our adapter class.
                SliderAdapter adapter = new SliderAdapter(getContext(), sliderDataArrayList);

                // below method is used to set auto cycle direction in left to
                // right direction you can change according to requirement.
                sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);

                // below method is used to
                // setadapter to sliderview.
                sliderView.setSliderAdapter(adapter);

                // below method is use to set
                // scroll time in seconds.
                sliderView.setScrollTimeInSec(3);

                // to set it scrollable automatically
                // we use below method.
                sliderView.setAutoCycle(true);

                // to start autocycle below method is used.
                sliderView.startAutoCycle();
            }




    private void init() {
        buttonBook=view.findViewById(R.id.btn_book);
    }
}
