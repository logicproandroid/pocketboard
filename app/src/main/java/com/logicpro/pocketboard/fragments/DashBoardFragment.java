package com.logicpro.pocketboard.fragments;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.activity.LevelActivity;

public class DashBoardFragment  extends  Fragment{
    private  View view;
    private ImageView imageMove;
    private FrameLayout frameLayout;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.setHasOptionsMenu(true);
        view = inflater.inflate(R.layout.fragment_dashborad, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init();
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(), LevelActivity.class);
                startActivity(intent);
            }
        });

      /*  ProgressBar mProgressBar = (ProgressBar)view. findViewById(R.id.pb_loading);
        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 80, 0);
        progressAnimator.setDuration(30000);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();
*/
        TranslateAnimation animation = new TranslateAnimation(0.0f, 80.0f,0.0f, 0.0f);
//  new TranslateAnimation(xFrom,xTo, yFrom,yTo)

        animation.setDuration(5000);  // animation duration
        animation.setRepeatCount(1);  // animation repeat count
        animation.setRepeatMode(1);   // repeat animation (left to right, right to left
//animation.setFillAfter(true);

        imageMove.startAnimation(animation);  // start animation



    }

    private void init() {
        imageMove=view.findViewById(R.id.imageMove);
        frameLayout=view.findViewById(R.id.frame_layout);
    }


}
