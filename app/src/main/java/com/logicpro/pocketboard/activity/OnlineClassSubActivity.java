package com.logicpro.pocketboard.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVSubjectYoutubeAdapter;
import com.logicpro.pocketboard.model.SubYouTubeForm;

import java.util.ArrayList;

public class OnlineClassSubActivity extends AppCompatActivity {
    private ArrayList<SubYouTubeForm> subYouTubeArrayList;
    private Toolbar toolbar;
    RecyclerView rvSubYoutube;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_sub);
        init();
        setUptoolBar();

        subYouTubeArrayList.add(new SubYouTubeForm("0", "English",R.drawable.live,"Sarita Shinde","12/02/2020"));
        subYouTubeArrayList.add(new SubYouTubeForm("1", "Geometry",R.drawable.banner1,"Ramesh Patil","15/02/2020"));
        subYouTubeArrayList.add(new SubYouTubeForm("2", "Hindi",R.drawable.banner13,"Kaveri Padakon","19/03/2020"));

        if (subYouTubeArrayList != null && subYouTubeArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
            rvSubYoutube.setLayoutManager(linearLayoutManager);
            RVSubjectYoutubeAdapter subjectYoutubeAdapter = new RVSubjectYoutubeAdapter(OnlineClassSubActivity.this, subYouTubeArrayList);
            rvSubYoutube.setHasFixedSize(true);
            rvSubYoutube.setNestedScrollingEnabled(false);
            rvSubYoutube.setAdapter(subjectYoutubeAdapter);
        }


    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void setUptoolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }

    private void init() {
        rvSubYoutube=findViewById(R.id.rv_sub_youtube);
        toolbar=findViewById(R.id.toolbar);
        subYouTubeArrayList=new ArrayList<>();
    }
}
