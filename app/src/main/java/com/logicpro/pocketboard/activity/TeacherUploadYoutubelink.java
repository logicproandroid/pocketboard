package com.logicpro.pocketboard.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.logicpro.pocketboard.R;

import com.logicpro.pocketboard.adapter.RVYoutubeUploadAdapter;

import com.logicpro.pocketboard.model.TeacherYoutubeLinkForm;

import java.util.ArrayList;

public class TeacherUploadYoutubelink extends AppCompatActivity {

    private ArrayList<TeacherYoutubeLinkForm> subYouTubeArrayList;
  //  private Toolbar toolbar;
  private FrameLayout frameLayout;
    private  View dialoglayout;
    private BottomSheetDialog dialog;

    RecyclerView rvSubYoutube;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_youtube_link);

        init();
      //  setUptoolBar();


        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                dialoglayout = li.inflate(R.layout.dialog_add_youtube_link, null);
                dialog = new BottomSheetDialog(TeacherUploadYoutubelink.this);
                dialog.setContentView(dialoglayout);

               /* Button saveTopping=dialoglayout.findViewById(R.id.btnSave);
                tvToppingTitle=dialoglayout.findViewById(R.id.tv_topping_title);
                tvToppingTitle.setVisibility(View.VISIBLE);
                Button cancelTopping=dialoglayout.findViewById(R.id.btnCancel);
                Button updateTopping=dialoglayout.findViewById(R.id.btnUpdate);*/
                //  mCircleImageView=dialoglayout.findViewById(R.id.img_toppings);
                // FrameLayout frameLayoutImage=dialoglayout.findViewById(R.id.iv_select_image);
               /* frameLayoutImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent  imageIntent=new Intent(getActivity(),ActivityImageTopping.class);
                        imageIntent.putExtra("pcId", mPcId);
                        startActivityForResult(imageIntent, IMAGE_RESULT_OK);
                    }
                });*/

               /* saveTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(etvToppingName.getText().toString().length()==0 || etvToppingPrice.getText().toString().length()==0){
                            alert();
                        }
                        else{


                            if(imageName==null)
                            {
                                mFinalImageName="null";
                            }
                            else
                            {
                                mFinalImageName=imageName.substring(imageName.lastIndexOf("/")+1);
                            }

                         //   saveToppingInformation();

                        }}*/


                  /*  private void saveToppingInformation() {

                        if (isValid()) {
                            initRetrofitCallBackForToppings();
                            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                            mRetrofitService = new RetrofitService(mResultCallBack, getActivity());
                            mRetrofitService.retrofitData(ADD_TOPPINGS, service.addToppings(etvToppingName.getText().toString(),
                                    mFinalImageName,
                                    Integer.parseInt(etvToppingPrice.getText().toString()),
                                    hotelId,
                                    mPcId));
                        }
                    }*/
                //  });


                /*cancelTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*/
                dialog.show();
            }
                                       });

        subYouTubeArrayList.add(new TeacherYoutubeLinkForm(R.drawable.live, "Radha Patil","English","12/02/2020","0","https://www.youtube.com/watch?v=3n1Jcr1vOhA","CBSE","English","10st std","10:30 AM"));
        subYouTubeArrayList.add(new TeacherYoutubeLinkForm(R.drawable.live, "Radha Patil","English","12/02/2020","0","https://www.youtube.com/watch?v=3n1Jcr1vOhA","CBSE","English","8th std", "12:30 PM"));
        subYouTubeArrayList.add(new TeacherYoutubeLinkForm(R.drawable.live, "Radha Patil","English","12/02/2020","0","https://www.youtube.com/watch?v=3n1Jcr1vOhA","CBSE","English","5th std","3:30PM"));

        if (subYouTubeArrayList != null && subYouTubeArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
            rvSubYoutube.setLayoutManager(linearLayoutManager);
            RVYoutubeUploadAdapter subjectYoutubeAdapter = new RVYoutubeUploadAdapter(TeacherUploadYoutubelink.this, subYouTubeArrayList);
            rvSubYoutube.setHasFixedSize(true);
            rvSubYoutube.setNestedScrollingEnabled(false);
            rvSubYoutube.setAdapter(subjectYoutubeAdapter);
        }



    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

   /* private void setUptoolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }*/

    private void init() {
        rvSubYoutube=findViewById(R.id.rv_upload_youtube);
     //   toolbar=findViewById(R.id.toolbar);
        subYouTubeArrayList=new ArrayList<>();
        frameLayout=findViewById(R.id.iv_add);
    }
}
