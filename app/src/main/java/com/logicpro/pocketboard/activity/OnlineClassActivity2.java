package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.logicpro.pocketboard.R;

public class OnlineClassActivity2 extends AppCompatActivity {
    private ImageView imageView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_activity_2);
        init();
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OnlineClassActivity2.this,OnlineClassActivity3.class);
                startActivity(intent);
            }
        });

    }

    private void init() {
        imageView=findViewById(R.id.img);
    }
}
