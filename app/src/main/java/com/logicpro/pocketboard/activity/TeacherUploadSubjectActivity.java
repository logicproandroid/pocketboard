package com.logicpro.pocketboard.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RvTeacherSUbDisplayAdapter;
import com.logicpro.pocketboard.model.TeacherUploadSubDisplay;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TeacherUploadSubjectActivity  extends AppCompatActivity {
    private RecyclerView RVTeacherSubject;
    private ArrayList<TeacherUploadSubDisplay> teacherUploadSubDisplayArrayList;
    private FrameLayout frameLayout;

    private  View dialoglayout;
    private  BottomSheetDialog dialog;
    private String imageName,mFinalImageName;
    private CircleImageView mCircleImageView;
    private EditText etvToppingName,etvToppingPrice;
    private TextView tvToppingTitle;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_sub_display);

        init();
        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                dialoglayout = li.inflate(R.layout.dialog_add_sudject1, null);
                dialog= new BottomSheetDialog(TeacherUploadSubjectActivity.this);
                dialog.setContentView(dialoglayout);

               /* Button saveTopping=dialoglayout.findViewById(R.id.btnSave);
                tvToppingTitle=dialoglayout.findViewById(R.id.tv_topping_title);
                tvToppingTitle.setVisibility(View.VISIBLE);
                Button cancelTopping=dialoglayout.findViewById(R.id.btnCancel);
                Button updateTopping=dialoglayout.findViewById(R.id.btnUpdate);*/
              //  mCircleImageView=dialoglayout.findViewById(R.id.img_toppings);
               // FrameLayout frameLayoutImage=dialoglayout.findViewById(R.id.iv_select_image);
               /* frameLayoutImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent  imageIntent=new Intent(getActivity(),ActivityImageTopping.class);
                        imageIntent.putExtra("pcId", mPcId);
                        startActivityForResult(imageIntent, IMAGE_RESULT_OK);
                    }
                });*/

               /* saveTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(etvToppingName.getText().toString().length()==0 || etvToppingPrice.getText().toString().length()==0){
                            alert();
                        }
                        else{


                            if(imageName==null)
                            {
                                mFinalImageName="null";
                            }
                            else
                            {
                                mFinalImageName=imageName.substring(imageName.lastIndexOf("/")+1);
                            }

                         //   saveToppingInformation();

                        }}*/


                  /*  private void saveToppingInformation() {

                        if (isValid()) {
                            initRetrofitCallBackForToppings();
                            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                            mRetrofitService = new RetrofitService(mResultCallBack, getActivity());
                            mRetrofitService.retrofitData(ADD_TOPPINGS, service.addToppings(etvToppingName.getText().toString(),
                                    mFinalImageName,
                                    Integer.parseInt(etvToppingPrice.getText().toString()),
                                    hotelId,
                                    mPcId));
                        }
                    }*/
              //  });


                /*cancelTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*/
                dialog.show();
            }


        });

        teacherUploadSubDisplayArrayList.add(new TeacherUploadSubDisplay("English","10th std","CBSC","Englist","12 chapter",R.drawable.ic_english));
        teacherUploadSubDisplayArrayList.add(new TeacherUploadSubDisplay("EVS","3rd std", "CBSC","Englist","10 chapter",R.drawable.ic_evs));
        teacherUploadSubDisplayArrayList.add(new TeacherUploadSubDisplay("Hindi","7 th std", "CBSC","Englist","8 chapter",R.drawable.ic_hindi));

        if (teacherUploadSubDisplayArrayList != null && teacherUploadSubDisplayArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            RVTeacherSubject.setLayoutManager(linearLayoutManager);
            RvTeacherSUbDisplayAdapter rvTeacherSub = new RvTeacherSUbDisplayAdapter(TeacherUploadSubjectActivity.this, teacherUploadSubDisplayArrayList);
            RVTeacherSubject.setHasFixedSize(true);
            RVTeacherSubject.setNestedScrollingEnabled(false);
            RVTeacherSubject.setAdapter(rvTeacherSub);
        }

    }

    private void init() {
        RVTeacherSubject=findViewById(R.id.rv_subject_list);
        teacherUploadSubDisplayArrayList=new ArrayList<>();
        frameLayout=findViewById(R.id.iv_add);
    }
}
