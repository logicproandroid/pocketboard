package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVSubjectAdapter;
import com.logicpro.pocketboard.model.SubjectForm;

import java.util.ArrayList;

public class OnlineClassActivity3 extends AppCompatActivity {

    private RecyclerView rvSubjects;
    private ArrayList<SubjectForm> subjectFormArrayList;
    private LinearLayout linearLayout;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_class_3);
        init();
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(OnlineClassActivity3.this,OnlineActivity4.class);
                startActivity(intent);
            }
        });

        subjectFormArrayList.add(new SubjectForm("0", "English",R.drawable.ic_english));
        subjectFormArrayList.add(new SubjectForm("1", "Geometry",R.drawable.ic_geometry));
        subjectFormArrayList.add(new SubjectForm("2", "Hindi",R.drawable.ic_hindi));

        if (subjectFormArrayList != null && subjectFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL,false);
            rvSubjects.setLayoutManager(linearLayoutManager);
            RVSubjectAdapter rvBadgeAdapter = new RVSubjectAdapter(this, subjectFormArrayList);
            rvSubjects.setHasFixedSize(true);
            rvSubjects.setNestedScrollingEnabled(false);
            rvSubjects.setAdapter(rvBadgeAdapter);
        }
    }

    private void init() {
        subjectFormArrayList=new ArrayList<>();
        rvSubjects=findViewById(R.id.rv_subject);
        linearLayout=findViewById(R.id.linearlayout);
    }
    }

