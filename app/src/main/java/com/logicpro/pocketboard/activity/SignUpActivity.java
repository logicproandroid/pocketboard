package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.Utils.RetrofitService;
import com.logicpro.pocketboard.interfaces.IResult;

import java.util.ArrayList;

public  class SignUpActivity  extends AppCompatActivity {
    private Spinner spGenedr, spClass, spBoard;
    private ArrayList<String> arrayListgender;
    private ArrayList<String> arrayListClass;
    private ArrayList<String> arrayListBoard;
    private String mGender, mClass, mBoard;
    private Button btnSignup;
    private  Intent intent;
    private RetrofitService mRetrofitService;
    private IResult mResultCallBack;

    private TextInputEditText etFullName,etEmail,etSchoolName,etAddress,etMobile;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_new);

        init();



        btnSignup=findViewById(R.id.btnSignup);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent=new Intent(SignUpActivity.this,SignUpTwoActivity.class);
                intent.putExtra("fullname",etFullName.getText().toString());
                intent.putExtra("email",etEmail.getText().toString());
                intent.putExtra("schoolname",etSchoolName.getText().toString());
                intent.putExtra("address",etAddress.getText().toString());
                intent.putExtra("mobile",etMobile.getText().toString());
                startActivity(intent);
            }
        });

        if(getIntent().getExtras() != null)
        {
            etFullName.setText("Ramesh Patil");
            etEmail.setText("ramesh.patil@gmail.com");
            etSchoolName.setText("Tiny tots app school Giri Nagar");
            etAddress.setText("Pune");
        }

      /*  init();
        arrayListgender.add("Gender");
        arrayListgender.add("Male");
        arrayListgender.add("Female");



        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, arrayListgender);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGenedr.setAdapter(stringArrayAdapter);

        spGenedr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mGender = spGenedr.getSelectedItem().toString();
                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        arrayListClass.add("1");
        arrayListClass.add("2");
        arrayListClass.add("3");
        arrayListClass.add("4");
        arrayListClass.add("5");
        arrayListClass.add("6");
        arrayListClass.add("7");
        arrayListClass.add("8");
        arrayListClass.add("9");
        arrayListClass.add("10");
        arrayListClass.add("11");
        arrayListClass.add("12");


        ArrayAdapter<String> stringArrayAdapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, arrayListClass);
        stringArrayAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spClass.setAdapter(stringArrayAdapter1);

        spClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mClass = spClass.getSelectedItem().toString();
                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        arrayListBoard.add("CBSE");
        arrayListBoard.add("ICSE");
        arrayListBoard.add("STATE");


        ArrayAdapter<String> stringArrayAdapter2 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item, arrayListBoard);
        stringArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBoard.setAdapter(stringArrayAdapter2);

        spBoard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mBoard = spBoard.getSelectedItem().toString();
                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void init() {
        spGenedr=findViewById(R.id.sp_gender);
        spClass=findViewById(R.id.sp_class);
        spBoard=findViewById(R.id.sp_education_board);

        arrayListgender=new ArrayList<>();
        arrayListClass=new ArrayList<>();
        arrayListBoard=new ArrayList<>();
    }*/
    }

    private void init() {
        etFullName=findViewById(R.id.etv_full_name);
        etEmail=findViewById(R.id.et_email);
        etSchoolName=findViewById(R.id.et_school_name);
        etAddress=findViewById(R.id.et_address);
        etMobile=findViewById(R.id.etv_mob);
    }
}
