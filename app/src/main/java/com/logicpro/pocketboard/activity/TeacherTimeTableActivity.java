package com.logicpro.pocketboard.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RvTimeTableAdapter;
import com.logicpro.pocketboard.model.TeacherTimeTable;

import java.util.ArrayList;
import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;

public class TeacherTimeTableActivity extends AppCompatActivity {
    private RecyclerView rvTimeTable;
    private ArrayList<TeacherTimeTable> teacherTimeTableArrayList;
    private View dialoglayout;
    private BottomSheetDialog dialog;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timetable);

        init();

        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);



        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                dialoglayout = li.inflate(R.layout.dialog_add_time_table, null);
                dialog= new BottomSheetDialog(TeacherTimeTableActivity.this);
                dialog.setContentView(dialoglayout);



                /*cancelTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*/
                dialog.show();


        }
        });

        teacherTimeTableArrayList.add(new TeacherTimeTable("English","12/02/2021","10:30 AM","Zoom","CBSC","English","7st std"));
        teacherTimeTableArrayList.add(new TeacherTimeTable("Hindi","12/02/2021","01:30 PM","Zoom","CBSC","English","7th std"));
        teacherTimeTableArrayList.add(new TeacherTimeTable("EVS","12/02/2021","03:30 AM","Zoom","CBSC","English","7th std"));

        if (teacherTimeTableArrayList != null && teacherTimeTableArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvTimeTable.setLayoutManager(linearLayoutManager);
            RvTimeTableAdapter rvTimeTableAdapter = new RvTimeTableAdapter(TeacherTimeTableActivity.this, teacherTimeTableArrayList);
            rvTimeTable.setHasFixedSize(true);
            rvTimeTable.setNestedScrollingEnabled(false);
            rvTimeTable.setAdapter(rvTimeTableAdapter);
        }
    }


    private void init() {
        rvTimeTable=findViewById(R.id.rv_timetable);
        teacherTimeTableArrayList=new ArrayList<>();
        frameLayout=findViewById(R.id.iv_add);
    }
}
