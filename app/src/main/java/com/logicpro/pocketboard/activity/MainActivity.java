package com.logicpro.pocketboard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.logicpro.pocketboard.R;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView tvToolBarTitle;
    private CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
      init();
      setUpToolBar();
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this, ActivityDashBorad.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        toolbar=findViewById(R.id.toolbar);
        tvToolBarTitle=toolbar.findViewById(R.id.tv_toolbar_title);
        cardView=findViewById(R.id.cardview);

    }

    private void setUpToolBar() {
       // tvToolBarTitle.setText("Pocket Borad");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }
}