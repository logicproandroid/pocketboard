package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.logicpro.pocketboard.R;

public class VerificationActivity extends AppCompatActivity {
    private LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        init();
        // setUpToolBar();
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VerificationActivity.this, MainActivity.class);
                startActivity(intent);

            }
        });
    }

    private void init() {
        linearLayout=findViewById(R.id.linearlayout);
    }
}



