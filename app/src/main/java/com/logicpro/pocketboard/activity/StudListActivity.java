package com.logicpro.pocketboard.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RvStudListAdapter;
import com.logicpro.pocketboard.adapter.RvUpComingClassAdapter;
import com.logicpro.pocketboard.model.StudentForm;
import com.logicpro.pocketboard.model.UpcomingClassForm;

import java.util.ArrayList;

public class StudListActivity extends AppCompatActivity {
    private RecyclerView rvStudList;
    private ArrayList<StudentForm> studentFormArrayList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stud_list);

        init();
        studentFormArrayList.add(new StudentForm("1","Raju Pattar", "1st std","CBSC","Englist","12/02/2020"));
        studentFormArrayList.add(new StudentForm("1","Geeta Shinde", "6th std","CBSC","Englist","01/02/2020"));
        studentFormArrayList.add(new StudentForm("1","Girish Gavade", "3rd std","CBSC","Englist","05/02/2020"));

        if (studentFormArrayList != null && studentFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvStudList.setLayoutManager(linearLayoutManager);
            RvStudListAdapter rvstudAdapter = new RvStudListAdapter(StudListActivity.this, studentFormArrayList);
            rvStudList.setHasFixedSize(true);
            rvStudList.setNestedScrollingEnabled(false);
            rvStudList.setAdapter(rvstudAdapter);
        }
    }

    private void init() {
        rvStudList=findViewById(R.id.rv_stud_list);
        studentFormArrayList=new ArrayList<>();
    }
}
