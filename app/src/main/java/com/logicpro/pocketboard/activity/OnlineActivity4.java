package com.logicpro.pocketboard.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVSubjectAdapter;
import com.logicpro.pocketboard.adapter.RvUpComingClassAdapter;
import com.logicpro.pocketboard.model.UpcomingClassForm;

import java.util.ArrayList;
import java.util.Calendar;

import devs.mulham.horizontalcalendar.HorizontalCalendar;

public class OnlineActivity4 extends AppCompatActivity {

    private RecyclerView rvComingClass;
    private ArrayList<UpcomingClassForm> upcomingClassFormArrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_4);


        upcomingClassFormArrayList = new ArrayList<>();
        rvComingClass = findViewById(R.id.rv_upcoming_class);
        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();


        upcomingClassFormArrayList.add(new UpcomingClassForm("10.30 AM", "English","Ragini Patil","Zoom","12/02/2020"));
        upcomingClassFormArrayList.add(new UpcomingClassForm("11.30 AM", "Hindi","Jeevan Shinde","GoogleMeet","12/02/2020"));
        upcomingClassFormArrayList.add(new UpcomingClassForm("12.30 AM", "Kannada","Geeta Ghokale","Zoom","12/02/2020"));

        if (upcomingClassFormArrayList != null && upcomingClassFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
            rvComingClass.setLayoutManager(linearLayoutManager);
            RvUpComingClassAdapter rvBadgeAdapter = new RvUpComingClassAdapter(OnlineActivity4.this, upcomingClassFormArrayList);
            rvComingClass.setHasFixedSize(true);
            rvComingClass.setNestedScrollingEnabled(false);
            rvComingClass.setAdapter(rvBadgeAdapter);
        }
    }
}
