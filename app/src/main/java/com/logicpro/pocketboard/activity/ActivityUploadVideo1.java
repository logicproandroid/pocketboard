package com.logicpro.pocketboard.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVSubjectClassAdapter;
import com.logicpro.pocketboard.model.SubjectClassModel;
import com.logicpro.pocketboard.model.SubjectForm;

import java.io.FileReader;
import java.util.ArrayList;

public class ActivityUploadVideo1 extends AppCompatActivity {

    private RecyclerView rvSubjects;
    private ArrayList<SubjectClassModel> subjectFormArrayList;
    private LinearLayout linearLayout;
    private FrameLayout frameLayout;
    private  View dialoglayout;
    private BottomSheetDialog dialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject_class);
        init();
       /* linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityUploadVideo1.this,OnlineActivity4.class);
                startActivity(intent);
            }
        });*/

        frameLayout.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                               dialoglayout = li.inflate(R.layout.dialog_add_upload_sub, null);
                                               dialog = new BottomSheetDialog(ActivityUploadVideo1.this);
                                               dialog.setContentView(dialoglayout);

               /* Button saveTopping=dialoglayout.findViewById(R.id.btnSave);
                tvToppingTitle=dialoglayout.findViewById(R.id.tv_topping_title);
                tvToppingTitle.setVisibility(View.VISIBLE);
                Button cancelTopping=dialoglayout.findViewById(R.id.btnCancel);
                Button updateTopping=dialoglayout.findViewById(R.id.btnUpdate);*/
                                               //  mCircleImageView=dialoglayout.findViewById(R.id.img_toppings);
                                               // FrameLayout frameLayoutImage=dialoglayout.findViewById(R.id.iv_select_image);
               /* frameLayoutImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent  imageIntent=new Intent(getActivity(),ActivityImageTopping.class);
                        imageIntent.putExtra("pcId", mPcId);
                        startActivityForResult(imageIntent, IMAGE_RESULT_OK);
                    }
                });*/

               /* saveTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(etvToppingName.getText().toString().length()==0 || etvToppingPrice.getText().toString().length()==0){
                            alert();
                        }
                        else{


                            if(imageName==null)
                            {
                                mFinalImageName="null";
                            }
                            else
                            {
                                mFinalImageName=imageName.substring(imageName.lastIndexOf("/")+1);
                            }

                         //   saveToppingInformation();

                        }}*/


                  /*  private void saveToppingInformation() {

                        if (isValid()) {
                            initRetrofitCallBackForToppings();
                            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                            mRetrofitService = new RetrofitService(mResultCallBack, getActivity());
                            mRetrofitService.retrofitData(ADD_TOPPINGS, service.addToppings(etvToppingName.getText().toString(),
                                    mFinalImageName,
                                    Integer.parseInt(etvToppingPrice.getText().toString()),
                                    hotelId,
                                    mPcId));
                        }
                    }*/
                                               //  });


                /*cancelTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*/
                                               dialog.show();
                                           }
                                       });

        subjectFormArrayList.add(new SubjectClassModel("0", "English","10th std",R.drawable.ic_english));
        subjectFormArrayList.add(new SubjectClassModel("1", "Geometry","5th std",R.drawable.ic_geometry));
        subjectFormArrayList.add(new SubjectClassModel("2", "Hindi","8th std",R.drawable.ic_hindi));

        if (subjectFormArrayList != null && subjectFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL,false);
            rvSubjects.setLayoutManager(linearLayoutManager);
            RVSubjectClassAdapter rvBadgeAdapter = new RVSubjectClassAdapter(this, subjectFormArrayList);
            rvSubjects.setHasFixedSize(true);
            rvSubjects.setNestedScrollingEnabled(false);
            rvSubjects.setAdapter(rvBadgeAdapter);
        }
    }

    private void init() {
        subjectFormArrayList=new ArrayList<>();
        rvSubjects=findViewById(R.id.rv_subject_list);
        linearLayout=findViewById(R.id.linearlayout);
        frameLayout=findViewById(R.id.iv_add);
    }



}
