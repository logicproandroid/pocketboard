package com.logicpro.pocketboard.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.logicpro.pocketboard.fragments.DashBoardFragment;
import com.logicpro.pocketboard.fragments.ExtraTestFragment;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.fragments.SettingFragment;
import com.logicpro.pocketboard.fragments.StatisticsFragment;

public class ActivityDashBorad  extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    private BottomNavigationView bottomNavigation;
    Fragment fragment = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activit_dasboard);
        init();

     //   bottomNavigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new DashBoardFragment())
                .commit();

        bottomNavigation.setOnNavigationItemSelectedListener(ActivityDashBorad.this);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new DashBoardFragment())
                .commit();
        bottomNavigation = findViewById(R.id.bottom_navigation);
       // bottomNavigation.inflateMenu(R.menu.bottom_navigation_menu);
       bottomNavigation.setOnNavigationItemSelectedListener(ActivityDashBorad.this);
        //bottomNavigation.getMenu().getItem(2).setChecked(true);

        //bottomNavigation.inflateMenu(R.menu.bottom_navigation_menu);

       // bottomNavigation.getMenu().getItem(2).setChecked(true);

    }

    /*BottomNavigationView.OnNavigationItemSelectedListener navigationItemSelectedListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.dashboard:
                            fragment = new DashBoardFragment();
                          ///  getSupportActionBar().setTitle("DashBoard");
                            return true;

                        case R.id.extra_exam:
                         //   openFragment(extraTestFragment.newInstance());
                            fragment = new ExtraTestFragment();
                         //   getSupportActionBar().setTitle("Extra Tests");
                            return true;

                        case R.id.statistics:
                            fragment = new StatisticsFragment();
                          //  getSupportActionBar().setTitle("Statistics");
                            return true;

                        case R.id.setting:
                            fragment = new SettingFragment();
                          //  getSupportActionBar().setTitle("Settings");
                            return true;
                    }
                    return openFragment(fragment);
                }
            };*/

    public boolean openFragment(Fragment fragment) {


        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.container, fragment);
            ft.detach(fragment);
            ft.attach(fragment);
            ft.addToBackStack(null);
            ft.commit();
            return true;
        }
        return false;

    }

    private void init() {
        bottomNavigation = findViewById(R.id.bottom_navigation);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.dashboard:
                fragment = new DashBoardFragment();
                ///  getSupportActionBar().setTitle("DashBoard");
                break;

            case R.id.extra_exam:
                //   openFragment(extraTestFragment.newInstance());
                fragment = new ExtraTestFragment();
                //   getSupportActionBar().setTitle("Extra Tests");
                break;

            case R.id.statistics:
                menuItem.setEnabled(true);
                fragment = new StatisticsFragment();
                //  getSupportActionBar().setTitle("Statistics");
                break;

            case R.id.setting:
                fragment = new SettingFragment();
                //  getSupportActionBar().setTitle("Settings");
                break;
        }
        return openFragment(fragment);
    }
}
