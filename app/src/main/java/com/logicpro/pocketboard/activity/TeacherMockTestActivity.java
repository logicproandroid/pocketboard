package com.logicpro.pocketboard.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVSubjectAdapter;
import com.logicpro.pocketboard.adapter.RVSubjectUploardAdapter;
import com.logicpro.pocketboard.model.SubjectForm;

import java.util.ArrayList;

public class TeacherMockTestActivity extends AppCompatActivity {
    private RecyclerView rvSubjectList;
    private ArrayList<SubjectForm> subjectFormArrayList;

    private View dialoglayout;
    private BottomSheetDialog dialog;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_subject);
        init();

        frameLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater li = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                dialoglayout = li.inflate(R.layout.dialog_add_mocktest, null);
                dialog= new BottomSheetDialog(TeacherMockTestActivity.this);
                dialog.setContentView(dialoglayout);

               /* Button saveTopping=dialoglayout.findViewById(R.id.btnSave);
                tvToppingTitle=dialoglayout.findViewById(R.id.tv_topping_title);
                tvToppingTitle.setVisibility(View.VISIBLE);
                Button cancelTopping=dialoglayout.findViewById(R.id.btnCancel);
                Button updateTopping=dialoglayout.findViewById(R.id.btnUpdate);*/
                //  mCircleImageView=dialoglayout.findViewById(R.id.img_toppings);
                // FrameLayout frameLayoutImage=dialoglayout.findViewById(R.id.iv_select_image);
               /* frameLayoutImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent  imageIntent=new Intent(getActivity(),ActivityImageTopping.class);
                        imageIntent.putExtra("pcId", mPcId);
                        startActivityForResult(imageIntent, IMAGE_RESULT_OK);
                    }
                });*/

               /* saveTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if(etvToppingName.getText().toString().length()==0 || etvToppingPrice.getText().toString().length()==0){
                            alert();
                        }
                        else{


                            if(imageName==null)
                            {
                                mFinalImageName="null";
                            }
                            else
                            {
                                mFinalImageName=imageName.substring(imageName.lastIndexOf("/")+1);
                            }

                         //   saveToppingInformation();

                        }}*/


                  /*  private void saveToppingInformation() {

                        if (isValid()) {
                            initRetrofitCallBackForToppings();
                            ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                            mRetrofitService = new RetrofitService(mResultCallBack, getActivity());
                            mRetrofitService.retrofitData(ADD_TOPPINGS, service.addToppings(etvToppingName.getText().toString(),
                                    mFinalImageName,
                                    Integer.parseInt(etvToppingPrice.getText().toString()),
                                    hotelId,
                                    mPcId));
                        }
                    }*/
                //  });


                /*cancelTopping.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });*/
                dialog.show();
            }


        });

        subjectFormArrayList.add(new SubjectForm("0", "English",R.drawable.ic_english));
        subjectFormArrayList.add(new SubjectForm("1", "Geometry",R.drawable.ic_geometry));
        subjectFormArrayList.add(new SubjectForm("2", "Hindi",R.drawable.ic_hindi));

        if (subjectFormArrayList != null && subjectFormArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            GridLayoutManager linearLayoutManager = new GridLayoutManager(this, 2);
            rvSubjectList.setLayoutManager(linearLayoutManager);
            RVSubjectUploardAdapter rvBadgeAdapter = new RVSubjectUploardAdapter(this, subjectFormArrayList);
            rvSubjectList.setHasFixedSize(true);
            rvSubjectList.setNestedScrollingEnabled(false);
            rvSubjectList.setAdapter(rvBadgeAdapter);
        }

    }

    private void init() {
        rvSubjectList=findViewById(R.id.rv_subject_list);
        subjectFormArrayList=new ArrayList<>();
        frameLayout=findViewById(R.id.iv_add);
    }
}
