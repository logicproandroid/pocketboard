package com.logicpro.pocketboard.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVQuestionNoAdapter;
import com.logicpro.pocketboard.model.QuestionNo;

import java.util.ArrayList;

public class QuestionActivity extends AppCompatActivity {

    private RecyclerView rvQuestionNoInfo;
    private ArrayList<QuestionNo> questionNoArrayList;
    private Toolbar toolbar;
    private TextView tvPrevious,tvNext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        init();
        setUpToolBar();

        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(QuestionActivity.this,AnswerActivity.class);
                startActivity(intent);
            }
        });

        tvPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create an alert builder
                AlertDialog.Builder builder
                        = new AlertDialog.Builder(QuestionActivity.this);
              //  builder.setTitle("Name");

                // set the custom layout
                final View customLayout = getLayoutInflater().inflate(R.layout.timeoff_layout, null);

                builder.setView(customLayout);
                getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                // add a button
                builder
                        .setPositiveButton(
                                "",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(
                                            DialogInterface dialog,
                                            int which) {

                                        // send data from the
                                        // AlertDialog to the Activity
                                        // EditText editText = customLayout.findViewById(R.id.editText);
                                       /* sendDialogDataToActivity(
                                                editText
                                                        .getText()
                                                        .toString());
                                    }*/
                                    }
                                });

                // create and show
                // the alert dialog
                AlertDialog dialog
                        = builder.create();
                dialog.show();
            }

            // Do something with the data

    });

        questionNoArrayList.add(new

    QuestionNo("1"));
        questionNoArrayList.add(new

    QuestionNo("2"));
        questionNoArrayList.add(new

    QuestionNo("3"));
        questionNoArrayList.add(new

    QuestionNo("4"));
        questionNoArrayList.add(new

    QuestionNo("5"));
        questionNoArrayList.add(new

    QuestionNo("6"));
        questionNoArrayList.add(new

    QuestionNo("7"));
        questionNoArrayList.add(new

    QuestionNo("8"));
        questionNoArrayList.add(new

    QuestionNo("9"));
        questionNoArrayList.add(new

    QuestionNo("10"));


        if(questionNoArrayList !=null&&questionNoArrayList.size()>0)

    {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        rvQuestionNoInfo.setLayoutManager(linearLayoutManager);
        RVQuestionNoAdapter rvQuestionNoAdapter = new RVQuestionNoAdapter(this, questionNoArrayList);
        rvQuestionNoInfo.setHasFixedSize(true);
        rvQuestionNoInfo.setNestedScrollingEnabled(false);
        rvQuestionNoInfo.setAdapter(rvQuestionNoAdapter);
    }

}

    private void setUpToolBar() {
        toolbar.setTitle("Test Name");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }

    private void init() {
        rvQuestionNoInfo = findViewById(R.id.rv_question_no);
        questionNoArrayList = new ArrayList<>();
        toolbar = findViewById(R.id.toolbar);
        tvPrevious = findViewById(R.id.tv_privious);
        tvNext=findViewById(R.id.tv_next);
    }
}
