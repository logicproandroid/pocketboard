package com.logicpro.pocketboard.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.adapter.RVLevelAdapter;
import com.logicpro.pocketboard.model.QuestionInfoModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public  class LevelActivity  extends AppCompatActivity {
    private Toolbar mToolBar;
    private CircleImageView imageView;
    private TextView toolBarTextView;
    private RecyclerView rvTestName;
    private ArrayList<QuestionInfoModel> questionInfoModelArrayList;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);

        init();
        setUpToolBar();
        Picasso.with(this).load(R.drawable.doctor5).into(imageView);

        //mQuestionName,mTime,mQuestionNo,mMarks,mRank,mTimeUp
        questionInfoModelArrayList.add(new QuestionInfoModel( "Maths Test","10:30 AM","1","50","1st","Time Up"));
        questionInfoModelArrayList.add(new QuestionInfoModel("English Test", "12:30 AM","1","50","2nd","Time Up"));
        questionInfoModelArrayList.add(new QuestionInfoModel("Kannada Test", "01:00 PM","1","50","7th","Time Up"));

        if (questionInfoModelArrayList != null && questionInfoModelArrayList.size() > 0) {
//            SearchItemArrayAdapter adapter = new SearchItemArrayAdapter(context, R.layout.autocomplete_layout, arrayListDoctorForm);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,RecyclerView.VERTICAL ,false);
            rvTestName.setLayoutManager(linearLayoutManager);
            RVLevelAdapter rvLevelAdapter = new RVLevelAdapter(this, questionInfoModelArrayList);
            rvTestName.setHasFixedSize(true);
            rvTestName.setNestedScrollingEnabled(false);
            rvTestName.setAdapter(rvLevelAdapter);
        }

    }

    private void setUpToolBar() {
        toolBarTextView.setText("Level 1");
       // setSupportActionBar(mToolBar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
    }

    private void init() {
    //    mToolBar=findViewById(R.id.toolbar);
        imageView=findViewById(R.id.image);
        toolBarTextView=findViewById(R.id.tvName);
        rvTestName=findViewById(R.id.rv_test_name);
        questionInfoModelArrayList=new ArrayList<>();
    }
}
