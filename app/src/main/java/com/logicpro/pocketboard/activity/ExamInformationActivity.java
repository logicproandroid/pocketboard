package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.logicpro.pocketboard.R;

public class ExamInformationActivity extends AppCompatActivity {
    private Button btnStart;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam_information);
        init();
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ExamInformationActivity.this,QuestionActivity.class);
                startActivity(intent);
            }
        });
    }

    private void init() {
        btnStart=findViewById(R.id.btn_start);
    }
}
