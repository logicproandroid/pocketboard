package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.logicpro.pocketboard.R;

public class LiveMockActivity extends AppCompatActivity {
    private Button btnContinue;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mock_live);
        init();
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LiveMockActivity.this,MainActivity.class);
                intent.putExtra("1","1");
                startActivity(intent);
            }
        });
    }

    private void init() {
        btnContinue=findViewById(R.id.btn_continue);
    }
}
