package com.logicpro.pocketboard.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.logicpro.pocketboard.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoardTeacherActivity extends AppCompatActivity {

    LinearLayout linerStudList,linearMockTest,linearSubject,linearTimeTable,linerUploadYoutubelink;
    private CircleImageView circleImageView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teacher_dashborad);
        init();


        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashBoardTeacherActivity.this,ActivityTeacherProfile.class);
                startActivity(intent);
            }
        });
        linerStudList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(DashBoardTeacherActivity.this,StudListActivity.class);
                startActivity(intent);
            }
        });

        linearMockTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(DashBoardTeacherActivity.this,TeacherMockTestActivity.class);
                startActivity(intent);
            }
        });





        linearSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(DashBoardTeacherActivity.this,TeacherUploadSubjectActivity.class);
                startActivity(intent);
            }
        });


        linearTimeTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(DashBoardTeacherActivity.this,TeacherTimeTableActivity.class);
                startActivity(intent);
            }
        });

        linerUploadYoutubelink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(DashBoardTeacherActivity.this,ActivityUploadVideo1.class);
                startActivity(intent);
            }
        });




    }

    private void init() {
        linerStudList=findViewById(R.id.linear1);
        linearMockTest=findViewById(R.id.linearlayout3);
        linearSubject=findViewById(R.id.linerlauyout2);
        linearTimeTable=findViewById(R.id.linearlayout4);
        linerUploadYoutubelink=findViewById(R.id.linerlayout6);
        circleImageView=findViewById(R.id.image);
    }
}
