package com.logicpro.pocketboard.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonObject;
import com.logicpro.pocketboard.R;
import com.logicpro.pocketboard.Utils.FilePath;
import com.logicpro.pocketboard.Utils.ImageFilePath;
import com.logicpro.pocketboard.Utils.RetrofitClientInstance;
import com.logicpro.pocketboard.Utils.RetrofitImageInstance;
import com.logicpro.pocketboard.Utils.RetrofitService;
import com.logicpro.pocketboard.interfaces.ApiService;
import com.logicpro.pocketboard.interfaces.IResult;
import com.logicpro.pocketboard.model.BoardForm;
import com.logicpro.pocketboard.model.ModelClassForm;
import com.logicpro.pocketboard.model.MudiumForm;
import com.logicpro.pocketboard.model.ProfileResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.logicpro.pocketboard.Utils.ConstantVariables.BOARD_LIST;
import static com.logicpro.pocketboard.Utils.ConstantVariables.CLASS_LIST;
import static com.logicpro.pocketboard.Utils.ConstantVariables.MUDIUM_LIST;
import static com.logicpro.pocketboard.Utils.ConstantVariables.PICK_GALLERY_IMAGE;
import static com.logicpro.pocketboard.Utils.ConstantVariables.REQUEST_PERMISSION;
import static com.logicpro.pocketboard.Utils.ConstantVariables.USER_REGISTER;

public class SignUpTwoActivity extends AppCompatActivity {

    private Spinner spGenedr, spClass, spBoard,spMudium;
    private ArrayList<String> arrayListgender;
    private ArrayList<ModelClassForm> arrayListClass;

    private ArrayList<String> arrayListBoard;
    private String mGender, mClass, mBoard,mFullname,mMob,mSchoolname,mAddress,mEmail,mMudium;
    private IResult mResultCallBack;
    private RetrofitService mRetrofitService;
    private  ArrayList<BoardForm> boardFormArrayList;
    private  ArrayList<MudiumForm> mudiumFormArrayList;
    private  int boardSelId,mudiumSelId,mClassId;
    private Button btnSave;
    private TextInputEditText etPassword;
    private DateFormat dfDate;
    private String finalFromdate,  databaseFormat="null";
    private  TextInputEditText etBirthDate;
    private ImageView imagePhoto;

    String responseValue="null";
    private String selectedFilePath, extension, selectedData, selectedImage, subString;
    private File selectedFile;
    private Bitmap bitmapImage;
    private String imageOldName;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_two);

        init();

        Intent intent=getIntent();
        mFullname=intent.getStringExtra("fullname");
        mMob=intent.getStringExtra("mobile");
        mSchoolname=intent.getStringExtra("schoolname");
        mAddress=intent.getStringExtra("address");
        mEmail=intent.getStringExtra("email");
       // mFullname=intent.getStringExtra("");

        imagePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    Intent imageIntent = new Intent();
                    imageIntent.setType("image/*");
                    imageIntent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(imageIntent, "Select Image"), PICK_GALLERY_IMAGE);
                } else {
                    requestPermission();
                }

            }
        });
        
        getBoardData();


        arrayListgender.add("Gender");
        arrayListgender.add("Male");
        arrayListgender.add("Female");

        ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrayListgender);
        stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGenedr.setAdapter(stringArrayAdapter);

        spGenedr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mGender = spGenedr.getSelectedItem().toString();
                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





        spClass.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mClass = spClass.getSelectedItem().toString();
                mClassId= Integer.parseInt(arrayListClass.get(i).getmClassId());

                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        spMudium.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mMudium = spMudium.getSelectedItem().toString();
                mudiumSelId= Integer.parseInt(mudiumFormArrayList.get(i).getMudiumId());
                //GenderSelectedId = i;

                getClassData(mudiumSelId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });






        spBoard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mBoard = spBoard.getSelectedItem().toString();
                boardSelId= Integer.parseInt(boardFormArrayList.get(i).getBoardId());
                getMudium(boardSelId);
               // Toast.makeText(SignUpTwoActivity.this, ""+boardFormArrayList.get(i).getBoardId(), Toast.LENGTH_SHORT).show();
                //GenderSelectedId = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        dfDate = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        Date c = Calendar.getInstance().getTime();
        final SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
        final String currentDate = df.format(c);
        etBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                DatePickerDialog picker = new DatePickerDialog(SignUpTwoActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = (dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                                getAge(year, (monthOfYear), dayOfMonth);
                                DateFormat inputFormat = new SimpleDateFormat("dd-MM-yyyy");


                                Date date1 = null;

                                try {
                                    date1 = inputFormat.parse(date);

                                    Log.e("date1", "" + date1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                databaseFormat=inputFormat.format(date1);

                                // Toast.makeText(getActivity(), ""+databaseFormat, Toast.LENGTH_SHORT).show();

                                Log.e("finalFromdate", "" + databaseFormat);
                                etBirthDate.setText(databaseFormat);

                                //CheckDates(currentDate, date);
                            }
                        }, year, month, day);
                try {
                    Date d = df.parse(currentDate);
                    picker.getDatePicker().setMaxDate(d.getTime());

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                picker.show();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRetrofitCallBack();
                ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
                mRetrofitService = new RetrofitService(mResultCallBack, SignUpTwoActivity.this);
                mRetrofitService.retrofitData(USER_REGISTER, (service.userRegister(mFullname,
                        mEmail,mMob,mSchoolname,
                        mAddress,
                        boardSelId,
                        mudiumSelId,
                        mClassId,
                        etBirthDate.getText().toString(),mGender,
                        "http://mahapolice.logicprosol.com//PocketBoardUs",etPassword.getText().toString()
                        )));

            }
        });
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this, new String[]{READ_EXTERNAL_STORAGE, WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION);

    }

    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (data == null) {
                //no data present
                return;
            }

            Uri selectedFileUri = data.getData();
            selectedFilePath = FilePath.getPath(this, selectedFileUri);
            String name = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);

            Bitmap categoryBitmap = null;
            try {
                categoryBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedFileUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String picturePath = null;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                picturePath = ImageFilePath.getPath(this, selectedFileUri);
            }

            selectedFile = new File(selectedFilePath);
            int file_size = Integer.parseInt(String.valueOf(selectedFile.length() / 1024));     //calculate size of image in KB
            // if (file_size <= 1024) {
            imagePhoto.setImageURI(selectedFileUri);
            extension = getFileExtension(selectedFile);
            uploadFile(name, selectedFile);

        }
    }

    public String getImageString(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        return Base64.encodeToString(imageBytes, Base64.DEFAULT);
    }

    private Bitmap exifInterface(String filePath, Bitmap bitmap) {
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert exif != null;
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);

        return rotateBitmap(bitmap, orientation);
    }

    private Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }


    private String getFileExtension(File selectedFile) {
        String fileName = selectedFile.getName();
        if (fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".") + 1);
        else return "";
    }



    private void uploadFile(String fileUri, File selectedFile) {
        Bitmap bmp = BitmapFactory.decodeFile(selectedFile.getAbsolutePath());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), selectedFile);
        MultipartBody.Part imagenPerfil = MultipartBody.Part.createFormData("image", selectedFile.getName(), requestFile);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData(fileUri, selectedFile.getName(), RequestBody.create(MediaType.parse("image/*"), selectedFile));
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData(fileUri, fileUri,
                RequestBody.create(MediaType.parse("image/*"), baos.toByteArray()));
        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), fileUri);
        ApiService uploadImage = RetrofitImageInstance.getRetrofitInstance().create(ApiService.class);
        mRetrofitService = new RetrofitService(mResultCallBack, this);
        Call<ProfileResponse> fileUpload = uploadImage.uploadImage(imagenPerfil, descBody);
        fileUpload.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {

                if (response.isSuccessful()) {
                    ProfileResponse jsonObject = response.body();
                    responseValue = jsonObject.getMessage("message");
                    Toast.makeText(SignUpTwoActivity.this, "Image Uploaded successfully!", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(SignUpTwoActivity.this, "Record not found", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Log.d("uploadimage", "Error occur " + t.getMessage());
                }
            }
        });
    }


    private void getAge(int year, int monthOfYear, int dayOfMonth) {
    }

    private void getClassData(int mudiumSelId) {
        initRetrofitCallBack();
        ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
        mRetrofitService = new RetrofitService(mResultCallBack, SignUpTwoActivity.this);
        mRetrofitService.retrofitData(CLASS_LIST, (service.getClassList(mudiumSelId)));
    }

    private void getMudium(int boardSelId) {
        initRetrofitCallBack();
        ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
        mRetrofitService = new RetrofitService(mResultCallBack, SignUpTwoActivity.this);
        mRetrofitService.retrofitData(MUDIUM_LIST, (service.getMudiamList(boardSelId)));
    }

    private void getBoardData() {
        initRetrofitCallBack();
        ApiService service = RetrofitClientInstance.getRetrofitInstance().create(ApiService.class);
        mRetrofitService = new RetrofitService(mResultCallBack, SignUpTwoActivity.this);
        mRetrofitService.retrofitData(BOARD_LIST, (service.getBoardList()));
    }


    private void initRetrofitCallBack() {
        mResultCallBack=new IResult() {
            @Override
            public void notifySuccess(int requestId, Response<JsonObject> response) {
               // Toast.makeText(SignUpTwoActivity.this, ""+response.body().getAsString(), Toast.LENGTH_SHORT).show();
                JsonObject jsonObject=response.body();
                String RespanceInfo=jsonObject.toString();

                switch (requestId)
                {
                    case BOARD_LIST:
                        try {
                            JSONObject object=new JSONObject(RespanceInfo);
                            int Stsatus=object.getInt("status");
                            if(Stsatus==1)
                            {
                                JSONArray jsonArray=object.getJSONArray("Data");
                                for(int i=0; i<jsonArray.length(); i++)
                                {
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    BoardForm boardForm=new BoardForm();
                                    boardForm.setBoardId(jsonObject1.getString("BoardId"));
                                    boardForm.setBoardName(jsonObject1.getString("Board"));
                                    boardFormArrayList.add(boardForm);
                                }

                                ArrayAdapter<BoardForm> stringArrayAdapter2 = new ArrayAdapter<BoardForm>(SignUpTwoActivity.this, android.R.layout.simple_spinner_dropdown_item, boardFormArrayList);
                                stringArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spBoard.setAdapter(stringArrayAdapter2);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case MUDIUM_LIST:
                        try {
                            JSONObject object=new JSONObject(RespanceInfo);
                            int Stsatus=object.getInt("status");
                            if(Stsatus==1)
                            {
                                JSONArray jsonArray=object.getJSONArray("Data");
                                for(int i=0; i<jsonArray.length(); i++)
                                {
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    MudiumForm mudiumForm=new MudiumForm();
                                    mudiumForm.setMudiumId(jsonObject1.getString("MediumId"));
                                    mudiumForm.setMudiumName(jsonObject1.getString("Medium"));
                                    mudiumFormArrayList.add(mudiumForm);
                                }

                                ArrayAdapter<MudiumForm> stringArrayAdapter2 = new ArrayAdapter<MudiumForm>(SignUpTwoActivity.this, android.R.layout.simple_spinner_dropdown_item, mudiumFormArrayList);
                                stringArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spMudium.setAdapter(stringArrayAdapter2);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case CLASS_LIST:
                        try {
                            JSONObject object=new JSONObject(RespanceInfo);
                            int Stsatus=object.getInt("status");
                            if(Stsatus==1)
                            {
                                JSONArray jsonArray=object.getJSONArray("Data");
                                for(int i=0; i<jsonArray.length(); i++)
                                {
                                    JSONObject jsonObject1=jsonArray.getJSONObject(i);
                                    ModelClassForm modelClassForm=new ModelClassForm();
                                    modelClassForm.setmClassId(jsonObject1.getString("ClassId"));
                                    modelClassForm.setmClassName(jsonObject1.getString("Class"));
                                    arrayListClass.add(modelClassForm);
                                }

                                ArrayAdapter<ModelClassForm> stringArrayAdapter2 = new ArrayAdapter<ModelClassForm>(SignUpTwoActivity.this, android.R.layout.simple_spinner_dropdown_item, arrayListClass);
                                stringArrayAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spClass.setAdapter(stringArrayAdapter2);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case USER_REGISTER:
                        try {
                            JSONObject object=new JSONObject(RespanceInfo);
                            int Stsatus=object.getInt("status");
                            if(Stsatus==1)
                            {
                                Toast.makeText(SignUpTwoActivity.this, "User Register Successfully....", Toast.LENGTH_SHORT).show();
                            }else
                            {
                                Toast.makeText(SignUpTwoActivity.this, "Something went wrong....", Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                }

            }

            @Override
            public void notifyError(int requestId, Throwable error) {
                Log.d("requestId","requestId"+requestId);
                Log.d("retrorfitErro","retrofitError"+error);


            }
        };
    }

    private void init() {
        spGenedr = findViewById(R.id.sp_gender);
        spClass = findViewById(R.id.sp_class);
        spBoard = findViewById(R.id.sp_education_board);
        spMudium=findViewById(R.id.sp_mudium);

        arrayListgender = new ArrayList<>();
        arrayListClass = new ArrayList<>();
       // arrayListBoard = new ArrayList<>();
        boardFormArrayList=new ArrayList<>();
        mudiumFormArrayList=new ArrayList<>();
        btnSave=findViewById(R.id.btn_save);
        etPassword=findViewById(R.id.et_password);
        etBirthDate=findViewById(R.id.etv_date_birth);
        imagePhoto=findViewById(R.id.stud_image);


    }
}

