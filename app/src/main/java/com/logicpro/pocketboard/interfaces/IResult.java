package com.logicpro.pocketboard.interfaces;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Response;

public interface IResult {

    void notifySuccess(int requestId, Response<JsonObject> response);
   // void notifySuccessArray(int requestId, Response<JsonArray> response);

    void notifyError(int requestId, Throwable error);
}
