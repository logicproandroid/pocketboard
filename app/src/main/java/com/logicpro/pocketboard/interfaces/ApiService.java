package com.logicpro.pocketboard.interfaces;

import com.google.gson.JsonObject;
import com.logicpro.pocketboard.model.ProfileResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {

    //String BASE_URL = "http://hmswebservice.shrivasundharadairy.com/";
    //  String BASE_URL="http://jaips.in/";
    String BASE_URL = "http://pocketboardapi.logicproems.com/"/*"https://www.doctor8.in/"*/;

    String BASE_URL1 = "http://mahapolice.logicprosol.com/";

    //  String BASE_URLIPD="http://192.168.1.209/api/IPD/";
    // @POST("Profile.asmx/UserProfile_Registration")
    @GET("api/PocketBoard/GetUserInfo")
    Call<JsonObject> userRegister(@Query("Fullname") String fullname,
                                  @Query("email") String email,
                                  @Query("mobileno") String mobileno,
                                  @Query("schoolname") String schoolname,
                                  @Query("address") String address,
                                  @Query("board") int board,
                                  @Query("Medium")int mediumId,
                                  @Query("Classid") int classId,
                                  @Query("dob") String  dob,
                                  @Query("gender") String geneder,
                                  @Query("imagepath") String imagepath,
                                  @Query("password") String password);

    @GET("api/Comman/GetBoard")
    Call<JsonObject>getBoardList();

    @GET("api/Comman/GetAllMedium")
    Call<JsonObject>getMudiamList(@Query("BoardId")int boardId);

    @GET("api/Comman/Getclass")
    Call<JsonObject>getClassList(@Query("MediumId")int mediumId);

    @Multipart
    @POST("Upload_Photo.php")
        // Call<String> uploadImage(@Part RequestBody file);
        //Call<ProfileResponse> uploadImage(@Body RequestBody file);
        // Call<ProfileResponse> uploadImage(@Part MultipartBody.Part image /*@Part("image") RequestBody image*/);
    Call<ProfileResponse>uploadImage(@Part MultipartBody.Part file, @Part("desc") RequestBody desc);

}
