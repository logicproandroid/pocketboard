package com.logicpro.pocketboard.model;

public class QuestionInfoModel {
    private  String mQuestionName,mTime,mQuestionNo,mMarks,mRank,mTimeUp;

    public QuestionInfoModel(String mQuestionName, String mTime, String mQuestionNo, String mMarks, String mRank, String mTimeUp) {
        this.mQuestionName = mQuestionName;
        this.mTime = mTime;
        this.mQuestionNo = mQuestionNo;
        this.mMarks = mMarks;
        this.mRank = mRank;
        this.mTimeUp = mTimeUp;
    }

    public QuestionInfoModel() {
    }

    public String getmQuestionName() {
        return mQuestionName;
    }

    public void setmQuestionName(String mQuestionName) {
        this.mQuestionName = mQuestionName;
    }

    public String getmTime() {
        return mTime;
    }

    public void setmTime(String mTime) {
        this.mTime = mTime;
    }

    public String getmQuestionNo() {
        return mQuestionNo;
    }

    public void setmQuestionNo(String mQuestionNo) {
        this.mQuestionNo = mQuestionNo;
    }

    public String getmMarks() {
        return mMarks;
    }

    public void setmMarks(String mMarks) {
        this.mMarks = mMarks;
    }

    public String getmRank() {
        return mRank;
    }

    public void setmRank(String mRank) {
        this.mRank = mRank;
    }

    public String getmTimeUp() {
        return mTimeUp;
    }

    public void setmTimeUp(String mTimeUp) {
        this.mTimeUp = mTimeUp;
    }
}
