package com.logicpro.pocketboard.model;

public class ModelClassForm {
    private  String mClassId,mClassName;

    public String getmClassId() {
        return mClassId;
    }

    public void setmClassId(String mClassId) {
        this.mClassId = mClassId;
    }

    public String getmClassName() {
        return mClassName;
    }

    public void setmClassName(String mClassName) {
        this.mClassName = mClassName;
    }

    @Override
    public String toString() {
        return mClassName;
    }
}
