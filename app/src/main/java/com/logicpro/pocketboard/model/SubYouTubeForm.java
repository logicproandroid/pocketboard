package com.logicpro.pocketboard.model;

public class SubYouTubeForm {
    private  int image;
    private  String teacherName,topic,updateDate,uploadId;

    public SubYouTubeForm() {
    }

    public SubYouTubeForm( String uploadId, String topic,int image, String teacherName, String updateDate) {
        this.uploadId = uploadId;
        this.topic = topic;
        this.image = image;
        this.teacherName = teacherName;

        this.updateDate = updateDate;

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }
}
