package com.logicpro.pocketboard.model;

public class MudiumForm {
    private  String mudiumId,mudiumName;

    public String getMudiumId() {
        return mudiumId;
    }

    public void setMudiumId(String mudiumId) {
        this.mudiumId = mudiumId;
    }

    public String getMudiumName() {
        return mudiumName;
    }

    public void setMudiumName(String mudiumName) {
        this.mudiumName = mudiumName;
    }

    @Override
    public String toString() {
        return mudiumName;
    }
}
