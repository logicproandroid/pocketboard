package com.logicpro.pocketboard.model;

import android.widget.TextView;

public class QuestionNo {
    private String questionNo;

    public QuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public QuestionNo() {
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }
}
