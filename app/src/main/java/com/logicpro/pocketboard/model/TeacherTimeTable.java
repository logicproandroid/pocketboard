package com.logicpro.pocketboard.model;

public class TeacherTimeTable  {
    private  String subjectName,date,time,callType,borad,medium,mClass;

    public TeacherTimeTable(String subjectName, String date, String time, String callType, String borad, String medium, String mClass) {
        this.subjectName = subjectName;
        this.date = date;
        this.time = time;
        this.callType = callType;
        this.borad = borad;
        this.medium = medium;
        this.mClass = mClass;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getBorad() {
        return borad;
    }

    public void setBorad(String borad) {
        this.borad = borad;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getmClass() {
        return mClass;
    }

    public void setmClass(String mClass) {
        this.mClass = mClass;
    }
}
