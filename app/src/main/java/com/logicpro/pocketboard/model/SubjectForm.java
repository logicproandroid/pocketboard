package com.logicpro.pocketboard.model;

public class SubjectForm {
    private  String subId,Subname;
    private  int image;

    public SubjectForm(String subId, String subname,int img ) {
        this.subId = subId;
        Subname = subname;
        this.image=img;
    }

    public SubjectForm() {
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getSubname() {
        return Subname;
    }

    public void setSubname(String subname) {
        Subname = subname;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
