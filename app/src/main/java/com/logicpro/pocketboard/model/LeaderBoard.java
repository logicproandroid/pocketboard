package com.logicpro.pocketboard.model;

public class LeaderBoard {
    private  String id,name,imgProfile,imgIcon,score;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgProfile() {
        return imgProfile;
    }

    public void setImgProfile(String imgProfile) {
        this.imgProfile = imgProfile;
    }

    public String getImgIcon() {
        return imgIcon;
    }

    public void setImgIcon(String imgIcon) {
        this.imgIcon = imgIcon;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public LeaderBoard(String id, String name, String imgProfile, String imgIcon, String score) {
        this.id = id;
        this.name = name;
        this.imgProfile = imgProfile;
        this.imgIcon = imgIcon;
        this.score = score;
    }
}
