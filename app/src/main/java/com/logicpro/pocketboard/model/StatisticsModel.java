package com.logicpro.pocketboard.model;

public class StatisticsModel {
    private  String id,score,testName;

    public StatisticsModel(String id, String score, String testName) {
        this.id = id;
        this.score = score;
        this.testName = testName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StatisticsModel() {
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
