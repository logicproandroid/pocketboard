package com.logicpro.pocketboard.model;

public class SliderData {
    private int imgUrl;

    public SliderData(int url1) {
        this.imgUrl = url1;
    }

    public int getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(int imgUrl) {
        this.imgUrl = imgUrl;
    }
}




