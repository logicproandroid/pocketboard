package com.logicpro.pocketboard.model;

public class UpcomingClassForm {
    private  String time,subject,teacherName,appName,date;

    public UpcomingClassForm() {
    }

    public UpcomingClassForm(String time, String subject, String teacherName, String appName, String date) {
        this.time = time;
        this.subject = subject;
        this.teacherName = teacherName;
        this.appName = appName;
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
