package com.logicpro.pocketboard.model;

public class TeacherYoutubeLinkForm {
    private  int image;
    private  String teacherName,topic,updateDate,uploadId,link,board,medium,mClass,time;

    public TeacherYoutubeLinkForm(int image, String teacherName, String topic, String updateDate, String uploadId, String link, String board, String medium, String mClass,String time) {
        this.image = image;
        this.teacherName = teacherName;
        this.topic = topic;
        this.updateDate = updateDate;
        this.uploadId = uploadId;
        this.link = link;
        this.board = board;
        this.medium = medium;
        this.mClass = mClass;
        this.time=time;
    }

    public TeacherYoutubeLinkForm() {
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getUploadId() {
        return uploadId;
    }

    public void setUploadId(String uploadId) {
        this.uploadId = uploadId;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBoard() {
        return board;
    }

    public void setBoard(String board) {
        this.board = board;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getmClass() {
        return mClass;
    }

    public void setmClass(String mClass) {
        this.mClass = mClass;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
