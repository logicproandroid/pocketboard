package com.logicpro.pocketboard.model;

import androidx.annotation.NonNull;

public class StudentForm {
    private  String studentid,name, classInfo, borad ,medium,subscriptionDate;

    public StudentForm(String studentid, String name, String classInfo, String borad, String medium, String subscriptionDate) {
        this.studentid = studentid;
        this.name = name;
        this.classInfo = classInfo;
        this.borad = borad;
        this.medium = medium;
        this.subscriptionDate = subscriptionDate;
    }

    public String getStudentid() {
        return studentid;
    }

    public void setStudentid(String studentid) {
        this.studentid = studentid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassInfo() {
        return classInfo;
    }

    public void setClassInfo(String classInfo) {
        this.classInfo = classInfo;
    }

    public String getBorad() {
        return borad;
    }

    public void setBorad(String borad) {
        this.borad = borad;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(String subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }
}
