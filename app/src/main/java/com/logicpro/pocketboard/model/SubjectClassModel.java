package com.logicpro.pocketboard.model;

public class SubjectClassModel {
    private  String subId,Subname,mclass;
    private  int image;

    public SubjectClassModel(String subId, String subname, String mclass, int image) {
        this.subId = subId;
        Subname = subname;
        this.mclass = mclass;
        this.image = image;
    }

    public String getSubId() {
        return subId;
    }

    public void setSubId(String subId) {
        this.subId = subId;
    }

    public String getSubname() {
        return Subname;
    }

    public void setSubname(String subname) {
        Subname = subname;
    }

    public String getMclass() {
        return mclass;
    }

    public void setMclass(String mclass) {
        this.mclass = mclass;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
