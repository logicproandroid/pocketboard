package com.logicpro.pocketboard.model;

public class TeacherUploadSubDisplay {
    private  String mSubjectName,mClassName,mBoradName,mMudiumName,totalChater;
    private  int icon;

    public TeacherUploadSubDisplay() {
    }

    public TeacherUploadSubDisplay(String mSubjectName, String mClassName, String mBoradName, String mMudiumName, String totalChater, int icon) {
        this.mSubjectName = mSubjectName;
        this.mClassName = mClassName;
        this.mBoradName = mBoradName;
        this.mMudiumName = mMudiumName;
        this.totalChater = totalChater;
        this.icon = icon;
    }

    public String getmSubjectName() {
        return mSubjectName;
    }

    public void setmSubjectName(String mSubjectName) {
        this.mSubjectName = mSubjectName;
    }

    public String getmClassName() {
        return mClassName;
    }

    public void setmClassName(String mClassName) {
        this.mClassName = mClassName;
    }

    public String getmBoradName() {
        return mBoradName;
    }

    public void setmBoradName(String mBoradName) {
        this.mBoradName = mBoradName;
    }

    public String getmMudiumName() {
        return mMudiumName;
    }

    public void setmMudiumName(String mMudiumName) {
        this.mMudiumName = mMudiumName;
    }

    public String getTotalChater() {
        return totalChater;
    }

    public void setTotalChater(String totalChater) {
        this.totalChater = totalChater;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
